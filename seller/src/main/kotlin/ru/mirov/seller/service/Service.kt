package ru.mirov.seller.service

import ru.mirov.seller.database.model.*
import ru.mirov.seller.telegram.screens.Screen
import java.util.UUID

interface Service {
    suspend fun findUser(tgId: String): TgBotUsers?
    suspend fun getScreen(id: Screen): TgScreens
    suspend fun getRegistrationScreen(msg: String, userName: String?, tgId: String): TgScreens
    suspend fun savePhone(msg: String, user: TgBotUsers)
    suspend fun saveEmail(msg: String, user: TgBotUsers)
    suspend fun saveInn(msg: String, user: TgBotUsers)
    suspend fun saveInstagram(msg: String, user: TgBotUsers)
    suspend fun faq(userType: UserType): List<Faq>
    suspend fun getActiveBuyouts(userId: String): List<OwnerBuyouts>
    suspend fun getTopBuyouts(userId: String): List<OwnerBuyouts>
    suspend fun selectGood(id: String): OwnerBuyouts
    suspend fun deleteGood(id: String)
    suspend fun savaClientBuyout(userId: String, buyoutId: UUID): ClientBuyouts?
    suspend fun getDraftBuyout(userId: String): OwnerBuyouts?
    suspend fun createDraftBuyout(userId: String)
    suspend fun updateBuyout(buyout: OwnerBuyouts)
    suspend fun startSearch(userId: String): TgScreens
    suspend fun endSearch(userId: String, msg: String?): List<OwnerBuyouts>
    suspend fun getClientBuyoutsForSeller(userId: String, type: UserType): List<ClientBuyouts>
    suspend fun deleteAccount(tgId: String)
    suspend fun getBuyers(): List<TgBotUsers>?
    suspend fun createSupportMessage(tgId: String)
    suspend fun getSupportMessage(tgId: String): SupportMessages?
    suspend fun updateSupportMessage(message: SupportMessages)
    suspend fun deleteSupportMessage(tgId: String)
    suspend fun getAllUsers(): List<TgBotUsers>
    suspend fun createSubscription(userId: String, searchValue: String?)
    suspend fun deleteSubscription(userId: String)
    suspend fun findSubscriptions(): List<BuyerSubscription>?
    suspend fun updateAllBuyers()
    suspend fun saveRate(currentTgId: String, ratedTgId: String, rate: Int)
    suspend fun getRate(userId: UUID): Double
    suspend fun instStatUpdate()
    suspend fun saveUserReview(review: UserToUserReview, clearOld: Boolean)
    suspend fun getUserReviews(userId: String): List<String>
    suspend fun findNewUserReview(userId: UUID): UserToUserReview?
    suspend fun findUserReviews(userId: String): List<String>
    suspend fun reviewExists(fromTgId: String, toTgId: String): Boolean
    suspend fun setSubscribeId(user: TgBotUsers)
    suspend fun updateSubscription(id: String)
    suspend fun activeSubscription(tgId: String): Boolean
    suspend fun saveNews(userId: String, message: String)
    suspend fun updateUserLastAction(userId: String, action: String, instruction: String? = null)
    suspend fun findTopBloggers(offset: Int): List<ButtonItem>
    suspend fun getLastNews(userId: String): BuyerNews?
    suspend fun updateUserName(chatId: String, userName: String?)
    suspend fun updateUserStatus(chatId: String, status: UserStatus)
    suspend fun updateInstagramFollowers(user: TgBotUsers?)
    suspend fun findAllForInform(): List<SubscribeInform>
    suspend fun updateBuyoutStatus(id: String, status: ClientBuyoutStatus)
    suspend fun massUpdate()
}
