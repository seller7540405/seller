package ru.mirov.seller.service.impl

import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.toList
import org.springframework.stereotype.Component
import ru.mirov.seller.database.model.*
import ru.mirov.seller.database.repository.*
import ru.mirov.seller.extensions.logger.Loggable
import ru.mirov.seller.instagram.InstagramService
import ru.mirov.seller.service.Service
import ru.mirov.seller.telegram.actions.Actions.REGISTRATION
import ru.mirov.seller.telegram.actions.Actions.SEARCH
import ru.mirov.seller.telegram.actions.Actions.SILENT
import ru.mirov.seller.telegram.buttons.Buttons
import ru.mirov.seller.telegram.screens.Screen
import java.lang.RuntimeException
import java.time.LocalDateTime
import java.util.UUID

@Component
class ServiceImpl(
    private val tgBotUsersRepository: TgBotUsersRepository,
    private val tgScreensRepository: TgScreensRepository,
    private val faqRepository: FaqRepository,
    private val ownerBuyoutsRepository: OwnerBuyoutsRepository,
    private val clientBuyoutsRepository: ClientBuyoutsRepository,
    private val supportMessagesRepository: SupportMessagesRepository,
    private val subscriptionRepository: BuyerSubscriptionRepository,
    private val instagramService: InstagramService,
    private val userRatingRepository: UserRatingRepository,
    private val usersInstStatRepository: UsersInstStatRepository,
    private val userToUserReviewRepository: UserToUserReviewRepository,
    private val buyerNewsRepository: BuyerNewsRepository,
    private val subscribeInformRepository: SubscribeInformRepository
): Service {
    companion object: Loggable
    override suspend fun findUser(tgId: String): TgBotUsers? = tgBotUsersRepository.findByTgId(tgId).firstOrNull()

    override suspend fun getScreen(id: Screen): TgScreens =
        tgScreensRepository.findById(id.toString()) ?: throw RuntimeException("No screen available")

    override suspend fun getRegistrationScreen(
        msg: String,
        userName: String?,
        tgId: String
    ): TgScreens {
        val user = initUser(userName, tgId, if (msg == Buttons.REGISTER_BUYER) UserType.BUYER else UserType.SELLER)
        return when (user.userDetails.type) {
            UserType.BUYER -> getScreen(Screen.NEED_INSTAGRAM)
            else -> getScreen(Screen.REGISTER_SELLER)
        }
    }

    override suspend fun savePhone(msg: String, user: TgBotUsers) {
        user.userDetails.phone = msg
        user.userDetails.status = UserStatus.NEED_EMAIL
        tgBotUsersRepository.save(user)
    }

    override suspend fun saveEmail(msg: String, user: TgBotUsers) {
        user.userDetails.email = msg
        user.userDetails.status = if (user.userDetails.type == UserType.SELLER) UserStatus.NEED_INN else UserStatus.NEED_INSTAGRAM
        tgBotUsersRepository.save(user)
    }

    override suspend fun saveInn(msg: String, user: TgBotUsers) {
        user.userDetails.inn = msg
        user.userDetails.status = UserStatus.CONFIRMED
        tgBotUsersRepository.save(user)
    }

    override suspend fun saveInstagram(msg: String, user: TgBotUsers) {
        val needInst = user.userDetails.socialLink.isNullOrEmpty()
        user.userDetails.socialLink = listOf(
            SocialLink(
                name = SocialType.INSTAGRAM,
                link = msg
            )
        )
        user.userDetails.status = UserStatus.CONFIRMED
        if (needInst)
            updateInstagramFollowers(user)
        tgBotUsersRepository.save(user)
    }

    override suspend fun updateInstagramFollowers(user: TgBotUsers?) {
        if (user == null || user.userDetails.type == UserType.SELLER)
            return
        val inst = user.userDetails.getInstagramLink()
            .replace("@", "")
            .replace("https://www.instagram.com/", "")
        if (inst.isEmpty())
            return
        if (user.userDetails.instRequestDate == null
            || user.userDetails.instRequestDate!!.plusDays(7).isBefore(LocalDateTime.now())) {
            val instagramDto = try {
                instagramService.call(inst)
            } catch (e: Exception) {
                logger.error("Error: ", e)
                null
            }
            if (instagramDto != null) {
                user.userDetails.followers = instagramDto.followers
                user.userDetails.following = instagramDto.following
            }
            user.userDetails.instRequestDate = LocalDateTime.now()
            tgBotUsersRepository.save(user)
        }
    }

    override suspend fun faq(userType: UserType): List<Faq> =
        faqRepository.findByType(
            if (userType == UserType.SELLER) FaqType.FOR_SELLER.toString() else FaqType.FOR_BUYER.toString()
        ).toList()

    override suspend fun getActiveBuyouts(userId: String): List<OwnerBuyouts> =
        ownerBuyoutsRepository
            .findAllByUserId(userId)
            .toList()
            .filter {
                it.buyoutDetails.endDate!!.isAfter(LocalDateTime.now())
            }

    override suspend fun getTopBuyouts(userId: String): List<OwnerBuyouts> =
        ownerBuyoutsRepository
            .findTop(userId)
            .toList()

    override suspend fun selectGood(id: String): OwnerBuyouts =
        ownerBuyoutsRepository.findById(UUID.fromString(id))!!

    override suspend fun deleteGood(id: String) {
        val dao = ownerBuyoutsRepository.findById(UUID.fromString(id))
        if (dao != null) {
            dao.buyoutDetails.status = BuyoutStatus.INACTIVE
            ownerBuyoutsRepository.save(dao.apply { isNew = false })
        }
    }

    override suspend fun savaClientBuyout(userId: String, buyoutId: UUID): ClientBuyouts? {
        if (clientBuyoutsRepository.findAllByUserId(userId, buyoutId.toString()).firstOrNull() != null)
            return null
        val buyout = ownerBuyoutsRepository.findById(buyoutId)
        if (buyout == null || buyout.buyoutDetails.status != BuyoutStatus.ACTIVE)
            return null
        val good = selectGood(buyoutId.toString())
        val user = findUser(userId)
        var inst = user?.userDetails?.socialLink?.get(0)?.link?.replace("@", "") ?: ""
        if (!inst.startsWith("https://www.instagram.com/"))
            inst = "https://www.instagram.com/$inst"
        var message = "Пользователь ${user?.userDetails?.nickName} оставил заявку на выкуп товара " +
                "*${good.buyoutDetails.name}*\n" +
                "Контакты:\n" +
                "${user?.userDetails?.phone ?: ""}\n" +
                "${user?.userDetails?.email ?: ""}\n" +
                "$inst\n"
        if (user?.userDetails?.followers != null)
            message += "\nПодписчики: ${user.userDetails.followers}"
        if (user?.userDetails?.following != null)
            message += "\nПодписок: ${user.userDetails.following}"
        return clientBuyoutsRepository.save(
            ClientBuyouts(
                id = UUID.randomUUID(),
                clientDetails = ClientDetail(
                    status = ClientBuyoutStatus.ORDERED,
                    buyoutId = buyoutId,
                    tgId = userId,
                    name = message,
                    buyerTgId = good.buyoutDetails.tgId
                )
            ).apply { isNew = true }
        )
    }

    override suspend fun getDraftBuyout(userId: String): OwnerBuyouts? =
        ownerBuyoutsRepository.findDraftByUserId(userId).firstOrNull()

    override suspend fun createDraftBuyout(userId: String) {
        val buyout = getDraftBuyout(userId)
        ownerBuyoutsRepository.save(
            OwnerBuyouts(
                id = buyout?.id ?: UUID.randomUUID(),
                buyoutDetails = BuyoutDetail(
                    name = "",
                    status = BuyoutStatus.WAITING,
                    buyoutDescription = "",
                    tgId = userId,
                    startDate = LocalDateTime.now(),
                    endDate = LocalDateTime.now().plusMonths(12)
                )
            ).apply { isNew = buyout == null }
        )
    }

    override suspend fun updateBuyout(buyout: OwnerBuyouts) {
        ownerBuyoutsRepository.save(buyout.apply { isNew = false })
    }

    override suspend fun startSearch(userId: String): TgScreens {
        updateLastAction(userId, SEARCH)
        return getScreen(Screen.ELASTIC_SEARCH_BUYOUTS)
    }

    override suspend fun endSearch(userId: String, msg: String?): List<OwnerBuyouts> {
        updateLastAction(userId, SILENT)
        val list = ownerBuyoutsRepository.findByValue(userId, msg, 150).toList()
        if (list.isNotEmpty())
            return list
        return ownerBuyoutsRepository.findByValue(userId, limit = 20).toList()
    }

    override suspend fun getClientBuyoutsForSeller(userId: String, type: UserType): List<ClientBuyouts> =
        (if (type == UserType.SELLER)
            clientBuyoutsRepository.findAllBySellerId(userId)
        else
            clientBuyoutsRepository.findAllByTgId(userId)).toList()

    override suspend fun deleteAccount(tgId: String) {
        val user = findUser(tgId)
        val buyouts = ownerBuyoutsRepository.findAllByUserId(tgId).toList()
        if (buyouts.isNotEmpty()) {
            buyouts.forEach {
                ownerBuyoutsRepository.delete(it)
            }
        }
        if (user != null)
            tgBotUsersRepository.delete(user)
    }

    override suspend fun getBuyers(): List<TgBotUsers>? =
        tgBotUsersRepository.findActiveBuyers().toList()

    override suspend fun createSupportMessage(tgId: String) {
        val messageDao = getSupportMessage(tgId)
        val dao = messageDao ?: SupportMessages(
            id = UUID.randomUUID(),
            createdAt = LocalDateTime.now(),
            updatedAt = LocalDateTime.now(),
            messageDetails = MessageDetails(tgId)
        )
        updateSupportMessage(dao.apply { isNew = messageDao == null })
    }

    override suspend fun getSupportMessage(tgId: String): SupportMessages? {
        val list = supportMessagesRepository.findByTgId(tgId).toList()
        return list.firstOrNull { it.messageDetails.message == null }
    }

    override suspend fun updateSupportMessage(message: SupportMessages) {
        supportMessagesRepository.save(message)
    }

    override suspend fun deleteSupportMessage(tgId: String) {
        val dao = getSupportMessage(tgId)
        if (dao != null)
            supportMessagesRepository.delete(dao)
    }

    override suspend fun getAllUsers(): List<TgBotUsers> = tgBotUsersRepository.findAll().toList()

    override suspend fun createSubscription(userId: String, searchValue: String?) {
        val dao = subscriptionRepository.findByTgId(userId).firstOrNull()
        subscriptionRepository.save(
            BuyerSubscription(
                id = dao?.id ?: UUID.randomUUID(),
                createdAt = LocalDateTime.now(),
                updatedAt = LocalDateTime.now(),
                tgId = userId,
                searchValue = searchValue
            ).apply { isNew = dao == null }
        )
    }

    override suspend fun deleteSubscription(userId: String) {
        val list = subscriptionRepository.findAllByTgId(userId).toList()
        if (list.isNotEmpty()) {
            list.forEach {
                subscriptionRepository.delete(it)
            }
        }
    }

    override suspend fun findSubscriptions(): List<BuyerSubscription>? = subscriptionRepository.findAllSubscriptions().toList()

    override suspend fun updateAllBuyers() {
        val list = tgBotUsersRepository.findAll().toList().filter {
            it.userDetails.type == UserType.BUYER &&
                    !it.userDetails.socialLink.isNullOrEmpty() &&
                    it.userDetails.following == null &&
                    it.userDetails.following == null
        }.take(10)
        list.forEach {
            saveInstagram(it.userDetails.socialLink?.get(0)?.link!!, it)
        }
    }

    override suspend fun saveRate(currentTgId: String, ratedTgId: String, rate: Int) {
        val currentUser = tgBotUsersRepository.findByTgId(currentTgId).firstOrNull() ?: return
        val ratedUser = tgBotUsersRepository.findByTgId(ratedTgId).firstOrNull() ?: return
        userRatingRepository.save(
            UserRating(
                rating = rate,
                userId = currentUser.id,
                ratedUserId = ratedUser.id
            )
        )
    }

    override suspend fun getRate(userId: UUID): Double {
        val list = userRatingRepository.findByRatedUserId(userId).toList()
        if (list.isEmpty())
            return 0.0
        return (list.sumOf { it.rating }.toDouble()).div(list.size)
    }

    override suspend fun instStatUpdate() {
        logger.info("Start stat update at {}", LocalDateTime.now())
        val list = usersInstStatRepository.findAll().toList()
        if (list.isEmpty()) {
            logger.info("End stat update with no result at {}", LocalDateTime.now())
            return
        }
        list.forEach {
            val user = tgBotUsersRepository.findById(it.id)
            if (user != null) {
                user.userDetails.following = it.following
                user.userDetails.followers = it.followers
                tgBotUsersRepository.save(user)
                usersInstStatRepository.delete(it)
            }
        }
        logger.info("End stat update at {}", LocalDateTime.now())
    }

    override suspend fun saveUserReview(review: UserToUserReview, clearOld: Boolean) {
        if (clearOld) {
            val oldDao =
                userToUserReviewRepository.findByUserIdAndFromUserId(review.userId, review.fromUserId).firstOrNull()
            if (oldDao != null)
                userToUserReviewRepository.delete(oldDao)
        }
        userToUserReviewRepository.save(
            review.apply { isNew = review.review == null }
        )
    }

    override suspend fun getUserReviews(userId: String): List<String> {
        val user = findUser(userId) ?: return emptyList()
        return userToUserReviewRepository.findByUserIdAndReviewIsNotNullOrderByCreatedAtDesc(user.id).toList()
            .map { "${it.review}\n-----------------\n" }
    }

    override suspend fun findNewUserReview(userId: UUID): UserToUserReview? =
        userToUserReviewRepository.findByFromUserIdAndReviewIsNull(userId).firstOrNull()

    override suspend fun findUserReviews(userId: String): List<String> {
        val user = findUser(userId) ?: return emptyList()
        return userToUserReviewRepository.findByUserIdAndReviewIsNotNull(user.id).toList()
            .map { "${it.review}\n-----------------\n" }
    }

    override suspend fun reviewExists(fromTgId: String, toTgId: String): Boolean {
        val fromUser = tgBotUsersRepository.findByTgId(fromTgId).firstOrNull() ?: return false
        val toUser = tgBotUsersRepository.findByTgId(toTgId).firstOrNull() ?: return false
        return userToUserReviewRepository.findByUserIdAndFromUserId(toUser.id, fromUser.id).firstOrNull() != null
    }

    override suspend fun setSubscribeId(user: TgBotUsers) {
        user.userDetails.subscribeId = user.userDetails.tgId
        tgBotUsersRepository.save(user)
    }

    override suspend fun updateSubscription(id: String) {
        val user = tgBotUsersRepository.findBySubscriptionId(id).firstOrNull() ?: return
        user.userDetails.subscribeStartDate = LocalDateTime.now()
        user.userDetails.subscribeEndDate = LocalDateTime.now().plusMonths(1)
        tgBotUsersRepository.save(user)
        if (user.userDetails.tgId != null)
            saveInform(user)
    }

    private suspend fun saveInform(user: TgBotUsers) {
        var subscribeInform = subscribeInformRepository.findByTgId(user.userDetails.tgId!!).firstOrNull()
        if (subscribeInform == null) {
            subscribeInform = SubscribeInform(
                id = UUID.randomUUID(),
                isSend = false,
                subscribeId = user.userDetails.tgId!!,
                subscribeEndDate = LocalDateTime.now().plusMonths(1),
                subscribeStartDate = LocalDateTime.now(),
                tgId = user.userDetails.tgId!!,
            )
            subscribeInform.isNew = true
        } else {
            subscribeInform.subscribeEndDate = LocalDateTime.now().plusMonths(1)
            subscribeInform.subscribeStartDate = LocalDateTime.now()
            subscribeInform.isSend = false
            subscribeInform.subscribeId = user.userDetails.tgId!!
            subscribeInform.isNew = false
        }
        subscribeInformRepository.save(subscribeInform)
    }

    override suspend fun activeSubscription(tgId: String): Boolean {
        val user = tgBotUsersRepository.findByTgId(tgId).firstOrNull() ?: return false
        if (user.userDetails.subscribeEndDate == null)
            return false
        return user.userDetails.subscribeEndDate!!.isAfter(LocalDateTime.now())
    }

    override suspend fun saveNews(userId: String, message: String) {
        val user = tgBotUsersRepository.findByTgId(userId).firstOrNull() ?: return
        buyerNewsRepository.save(
            BuyerNews(
                id = UUID.randomUUID(),
                newsMessage = message,
                isSend = true,
                userId = user.id
            ).apply { isNew = true }
        )
    }

    override suspend fun updateUserLastAction(userId: String, action: String, instruction: String?) {
        updateLastAction(userId, action, instruction)
    }

    override suspend fun findTopBloggers(offset: Int): List<ButtonItem> {
        val list = tgBotUsersRepository.findTopBloggers(offset, 10)
        if (list.isEmpty())
            return emptyList()
        val returnList = list.map {
            var name = it.userDetails.getTgNickName() ?: it.userDetails.getInstagramLink()
            if (name.contains("null"))
                name = it.userDetails.tgId ?: "Блоггер"
            ButtonItem(
                name = name,
                action = "get_seller_card_${it.userDetails.tgId}"
            )
        }.toMutableList()
        if (offset <= 700) {
            returnList.add(
                ButtonItem(
                    name = "Далее >>>",
                    action = "top_bloggers_${offset + 10}"
                )
            )
            if (offset > 0) {
                returnList.addFirst(
                    ButtonItem(
                        name = "<<< Назад",
                        action = "top_bloggers_${offset - 10}"
                    )
                )
            }
        }
        return returnList
    }

    override suspend fun getLastNews(userId: String): BuyerNews? {
        val user = tgBotUsersRepository.findByTgId(userId).firstOrNull() ?: return null
        return buyerNewsRepository.findNews(user.id).firstOrNull()
    }

    override suspend fun updateUserName(chatId: String, userName: String?) {
        if (userName == null)
            return
        val userNameForUpdate = "@$userName"
        val user = tgBotUsersRepository.findByTgId(chatId).firstOrNull() ?: return
        if (userNameForUpdate != user.userDetails.nickName) {
            logger.info("Update user_id {} user_name_old {} user_name_new {}", user.id, user.userDetails.nickName, userNameForUpdate)
            user.userDetails.nickName = userNameForUpdate
            user.userDetails.nickNameChangeCount = user.userDetails.nickNameChangeCount?.plus(1)
            tgBotUsersRepository.save(user)
        }
    }

    override suspend fun updateUserStatus(chatId: String, status: UserStatus) {
        val user = tgBotUsersRepository.findByTgId(chatId).firstOrNull() ?: return
        user.userDetails.status = status
        tgBotUsersRepository.save(user)
    }

    override suspend fun findAllForInform(): List<SubscribeInform> {
        val list = subscribeInformRepository.findAllForInform()
        list.forEach {
            it.isSend = true
            subscribeInformRepository.save(it.apply { isNew = false })
        }
        return list
    }

    override suspend fun updateBuyoutStatus(id: String, status: ClientBuyoutStatus) {
        val buyout = clientBuyoutsRepository.findById(UUID.fromString(id)) ?: return
        buyout.clientDetails.status = status
        clientBuyoutsRepository.save(buyout)
    }

    override suspend fun massUpdate() {
        val list = tgBotUsersRepository.findAll().toList()
        list.forEach {
            it.userDetails.subscribeId = it.userDetails.tgId
            it.userDetails.subscribeStartDate = LocalDateTime.now()
            it.userDetails.subscribeEndDate = LocalDateTime.of(2024, 7, 1, 0, 0)
            tgBotUsersRepository.save(it)
            saveInform(it)
        }

    }

    private  suspend fun updateLastAction(userId: String, action: String, instruction: String? = null) {
        val user = findUser(userId)
        if (user != null) {
            user.userDetails.lastAction = action
            if (instruction != null)
                user.userDetails.instruction = instruction
            tgBotUsersRepository.save(user.apply { isNew = false })
        }
    }

    private suspend fun initUser(userName: String?, tgId: String, buyer: UserType): TgBotUsers {
        val user = findUser(tgId)
        if (user != null)
            return user
        var stDate: LocalDateTime? = null
        var endDate: LocalDateTime? = null
        if (LocalDateTime.now().isBefore(LocalDateTime.of(2024, 7, 1, 0, 0))) {
            stDate = LocalDateTime.now()
            endDate = LocalDateTime.of(2024, 7, 1, 23, 59)
        }
        return tgBotUsersRepository.save(
            TgBotUsers(
                id = UUID.randomUUID(),
                userDetails = UserDetails(
                    tgId = tgId,
                    lastAction = REGISTRATION,
                    status = if (buyer == UserType.SELLER) UserStatus.CONFIRMED else UserStatus.NEED_INSTAGRAM,
                    nickName = userName,
                    type = buyer,
                    byOutsCount = 0,
                    subscribeId = tgId,
                    subscribeStartDate = stDate,
                    subscribeEndDate = endDate
                )
            ).apply { isNew = true }
        )
    }
}
