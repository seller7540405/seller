package ru.mirov.seller.telegram.client

import kotlinx.coroutines.slf4j.MDCContext
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import org.telegram.telegrambots.bots.TelegramLongPollingBot
import org.telegram.telegrambots.meta.api.methods.ActionType
import org.telegram.telegrambots.meta.api.methods.GetFile
import org.telegram.telegrambots.meta.api.methods.groupadministration.GetChat
import org.telegram.telegrambots.meta.api.objects.Message
import org.telegram.telegrambots.meta.api.objects.Update
import ru.mirov.seller.database.model.*
import ru.mirov.seller.database.repository.TgBotUsersRepository
import ru.mirov.seller.extensions.logger.Loggable
import ru.mirov.seller.parser.MarketPlaceParser
import ru.mirov.seller.service.Service
import ru.mirov.seller.telegram.actions.Actions.ACTIONS
import ru.mirov.seller.telegram.actions.Actions.SEARCH
import ru.mirov.seller.telegram.buttons.Buttons
import ru.mirov.seller.telegram.commands.Commands
import ru.mirov.seller.telegram.reaction.BotReaction
import ru.mirov.seller.telegram.screens.Screen
import ru.mirov.seller.utils.*
import java.io.File
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit
import java.util.*

@Component
class TelegramClient(
    @Value("\${application.telegram.token}") private val botToken: String,
    @Value("\${application.telegram.user}") private val botUser: String,
    @Value("\${application.video.buyer}") private val videoBuyerUrl: String,
    @Value("\${application.video.seller}") private val videoSellerUrl: String,
    @Value("\${application.video.buyer-subscribe}") private val videoBuyerSubscribeUrl: String,
    @Value("\${application.video.seller-subscribe}") private val videoSellerSubscribeUrl: String,
    @Value("\${application.telegram.ids}")
    private var ids: List<String>,
    private val service: Service,
    private val reaction: BotReaction,
    private val parser: Map<String, MarketPlaceParser>, private val tgBotUsersRepository: TgBotUsersRepository
): TelegramLongPollingBot(botToken) {
    companion object: Loggable {
        private const val WB = "wildberries"
        private const val LAMODA = "lamoda"
        private const val OZON = "ozon"

        private const val SELECT_GOOD = "select_good_"
        private const val DELETE_GOOD = "delete_good_"
        private const val GET_GOOD = "get_good_"
        private const val SELECT_BUYER = "get_seller_card_"
        private const val SELECT_SELLER = "get_buyer_card_"

        private const val DELETE_ACCOUNT = "delete_account"
        private const val REMAIN_ACCOUNT = "remain_account"

        private const val DEFAULT_INSTRUCTION = "Обратитесь к продавцу для получения инструкций по выкупу товара %s"

        private const val TG_LINK = "https://t.me/"

        private const val RATE_BUYER = "rate_buyer_"
        private const val RATE_SELLER = "rate_seller_"
        private const val REVIEW_BUYER = "review_buyer_"
        private const val REVIEW_SELLER = "review_seller_"

        private const val LIST_REVIEW_BUYER = "list_review_buyer_"

        private const val DECLINED_MY_BUYOUT = "declined_my_buyout_"
        private const val DONE_MY_BUYOUT = "done_my_buyout_"
    }

    override fun getBotUsername(): String = botUser

    @Scheduled(cron = "0 0 12 * * *")
    fun runSubscription() {
        doLaunch(mdcContext = MDCContext()) {
            subscription()
        }
    }

    @Scheduled(cron = "0 */15 * ? * *")
    fun runInstStat() {
        doLaunch(mdcContext = MDCContext()) {
            service.instStatUpdate()
        }
    }

    private suspend fun subscription() {
        val list = service.findSubscriptions()
        if (list.isNullOrEmpty())
            return
        list.forEach {
            try {
                endSearch(it.tgId, it.searchValue)
            } catch (e: Exception) {
                logger.error("Error sending subscription to user ${it.tgId}: ", e)
            }
        }
    }

    override fun onUpdateReceived(update: Update?) {

        doLaunch(mdcContext = MDCContext()) {

            val threadId = update?.message?.messageThreadId ?:
            if (update?.callbackQuery?.message != null)
                    (update.callbackQuery?.message as Message).messageThreadId
            else
                null
            if (threadId == null) {
                val tgId = (update?.message?.chatId ?: update?.callbackQuery?.from?.id).toString()
                execute(reaction.sendChatAction(tgId, ActionType.TYPING))
                try {
                    val messageId = update?.message?.messageId ?: (update?.callbackQuery?.message as Message).messageId
                    logger.info("MessageId {}", messageId)
                    val mList = mutableListOf<Int>()
                    if (messageId != null) {
                        for (i in messageId - 95 until messageId - 1) {
                            mList.add(i)
                        }
                    }
                    execute(reaction.deleteMessage(tgId, mList))
                    logger.info("Messages: {}", mList)
                } catch (e: Exception) {
                    logger.error("Error: ", e)
                }

                Thread.sleep(200)
                val user = service.findUser(tgId)

                if (user?.userDetails?.status == UserStatus.BLOCKED) {
                    execute(reaction.sendText(tgId, "Вам заблокирован доступ в Бартер бот"))
                } else {
                    service.updateInstagramFollowers(user)
                    processNotBlockedClient(update, user, tgId)
                }
            }
        }
    }

    private suspend fun processNotBlockedClient(update: Update?, user: TgBotUsers?, tgId: String) {
        val msg = update?.message?.text ?: update?.callbackQuery?.data ?: update?.message?.caption
        val buttons = Buttons.BUTTONS.contains(msg) || (msg?: "").startsWith("/")
        if (!buttons && user != null &&
            msg != null &&
            user.userDetails.lastAction != null &&
            ACTIONS.contains(user.userDetails.lastAction)) {
            processLastAction(user.userDetails.tgId!!, user.userDetails.lastAction!!, msg, update)
        } else {
            if (user != null && msg != null && user.userDetails.lastAction != SEARCH) {
                user.userDetails.lastAction = msg
                user.updatedAt = LocalDateTime.now()
                tgBotUsersRepository.save(user.apply { isNew = false })
            }

            if (user != null &&
                msg != null &&
                !msg.startsWith("/") &&
                !msg.contains("_")
            ) {
                val review = service.findNewUserReview(user.id)
                if (review != null) {
                    review.review = msg
                    processReviewUser(user, review = review)
                    return
                }
            }

            if (ids.contains(tgId) && (msg != null && msg.contains("News"))) {
                if (msg.contains("News_Seller:")) {
                    sendNews(msg.replace("News_Seller:", ""), UserType.SELLER, update = update)
                } else if (msg.contains("News_Buyer:")) {
                    sendNews(msg.replace("News_Buyer:", ""), UserType.BUYER, update = update)
                } else {
                    sendNews(msg.replace("News:", ""), update = update)
                }
            } else if (msg != null && msg.startsWith("Искать", ignoreCase = true)) {
                createSubscription(tgId, msg)
            } else {
                var registration = true
                val stop = msg != null && msg.contains("*Добавлен новый товар:*")
                if (msg != null) {
                    if (!stop) {
                        if (msg.startsWith(Commands.INSTRUCTION)) {
                            service.deleteSupportMessage(tgId)
                            execute(reaction.sendScreen(tgId, service.getScreen(Screen.INSTRUCTION_SCREEN)))
                        } else {
                            if (Buttons.BUTTONS.contains(msg)) {
                                processButtons(update!!, tgId, msg)
                                registration = false
                            } else if (msg.startsWith("my_card")) {
                                processSelectBuyer(tgId, tgId)
                            } else if (msg.startsWith("change_my_instagram")) {
                                processChangeInstagram(tgId)
                            } else if (msg.startsWith("top_bloggers_")) {
                                val offset = msg.replace("top_bloggers_", "").toInt()
                                execute(reaction.sendText(tgId, "Список топ блогеров", service.findTopBloggers(offset)))
                            } else if (msg.startsWith(SELECT_GOOD)) {
                                processGoodCard(tgId, msg.replace(SELECT_GOOD, ""))
                            } else if (msg.startsWith("all_seller_goods_")) {
                                getOwnerGoods(tgId, msg.replace("all_seller_goods_", ""))
                            } else if (msg.startsWith(SELECT_BUYER)) {
                                processSelectBuyer(tgId, msg.replace(SELECT_BUYER, ""))
                            } else if (msg.startsWith(SELECT_SELLER)) {
                                processSelectSeller(tgId, msg.replace(SELECT_SELLER, ""))
                            } else if (msg.startsWith(LIST_REVIEW_BUYER)) {
                                processBuyerReviews(tgId, msg.replace(LIST_REVIEW_BUYER, ""))
                            } else if (msg.startsWith(DECLINED_MY_BUYOUT)) {
                                processDeclinedBuyout(tgId, msg.replace(DECLINED_MY_BUYOUT, ""))
                            } else if (msg.startsWith(DONE_MY_BUYOUT)) {
                                processDoneBuyout(tgId, msg.replace(DONE_MY_BUYOUT, ""))
                            } else if (msg.startsWith(DELETE_GOOD)) {
                                processDeleteGood(tgId, msg.replace(DELETE_GOOD, ""))
                            } else if (msg.startsWith(GET_GOOD)) {
                                processGetGood(tgId, msg.replace(GET_GOOD, ""))
                            } else if (msg.startsWith(DELETE_ACCOUNT)) {
                                processDeleteAccount(tgId)
                            } else if (msg.startsWith(REMAIN_ACCOUNT)) {
                                mainScreen(tgId, user!!)
                            } else if (msg.startsWith(RATE_BUYER) || msg.startsWith(RATE_SELLER)) {
                                processRateUser(
                                    tgId,
                                    if (msg.startsWith(RATE_BUYER)) msg.replace(RATE_BUYER, "") else msg.replace(
                                        RATE_SELLER,
                                        ""
                                    )
                                )
                            } else if (msg.startsWith(REVIEW_BUYER) || msg.startsWith(REVIEW_SELLER)) {
                                processReviewUser(
                                    user!!,
                                    if (msg.startsWith(REVIEW_BUYER)) msg.replace(REVIEW_BUYER, "") else msg.replace(
                                        REVIEW_SELLER,
                                        ""
                                    )
                                )
                            } else if (msg.startsWith("make_rate_")) {
                                processMakeRate(tgId, msg, user!!)
                            } else if (user != null && !msg.startsWith("/")) {
                                if (user.userDetails.lastAction == SEARCH) {
                                    endSearch(tgId, msg)
                                } else {
                                    val buyout = service.getDraftBuyout(tgId)
                                    processBuyout(buyout, tgId, msg)
                                    processSupportMessage(service.getSupportMessage(tgId), msg, tgId)
                                    processRegistration(tgId, user, msg, user.userDetails.status!!)
                                }
                            }
                        }
                    }
                }
                if (!stop) {
                    if (msg == null || !msg.startsWith(Commands.INSTRUCTION)) {
                        if (registration && user == null) {
                            service.deleteSupportMessage(tgId)
                            execute(reaction.sendScreen(tgId, service.getScreen(Screen.CHOOSE_TYPE)))
                        } else if (msg != null && user != null) {
                            processCommands(msg, user)
                        }
                    }
                }
            }
        }
    }

    private suspend fun processChangeInstagram(tgId: String) {
        service.updateUserLastAction(tgId, "change_my_instagram")
        execute(reaction.sendText(tgId, "Укажите ваш аккаунт в instagram (например ссылку (https://www.instagram.com/Mirov_artur) или никнейм через @ (@Mirov_artur))"))
    }

    private suspend fun processDeclinedBuyout(tgId: String, id: String) {
        service.updateBuyoutStatus(id, ClientBuyoutStatus.DECLINED)
        execute(reaction.sendText(tgId, "Заявка отклонена"))
    }

    private suspend fun processDoneBuyout(tgId: String, id: String) {
        service.updateBuyoutStatus(id, ClientBuyoutStatus.APPROVED)
        execute(reaction.sendText(tgId, "Заявка исполнена"))
    }

    private suspend fun processLastAction(tgId: String, lastAction: String, msg: String, update: Update? = null) {
        when(lastAction) {
            "buyer_instruction_create_start" -> {
                service.updateUserLastAction(tgId, "buyer_instruction_create_end", msg)
                execute(reaction.sendText(tgId, "Спасибо! Ваша инструкция добавлена\nТеперь продавцам будет гораздо проще с Вами работать :)"))
            }
            "buyer_news_create_start" -> {
                val user = service.findUser(tgId)
                service.saveNews(tgId, msg)
                service.updateUserLastAction(tgId, "buyer_news_create_end")
                execute(reaction.sendText(tgId, "Ваша новость сохранена и будет отправлена в течении нескольких секунд"))
                if (user?.userDetails?.type == UserType.SELLER) {
                    sendNews(msg, UserType.BUYER, update, isMarketing = true, nickName = user.userDetails.nickName, tgId = user.userDetails.tgId)
                } else {
                    sendNews(msg, UserType.SELLER, update, isMarketing = true, nickName = user?.userDetails?.nickName, tgId = user?.userDetails?.tgId)
                }
                mainScreen(tgId, service.findUser(tgId)!!)
            }
            "change_my_instagram" -> {
                service.saveInstagram(msg, service.findUser(tgId)!!)
                service.updateUserLastAction(tgId, "change_my_instagram_end")
                execute(reaction.sendText(tgId, "Ссылка на ваш инстаграм успешно изменена"))
                processSelectBuyer(tgId, tgId)
            }
        }
    }

    private suspend fun processBuyerReviews(tgId: String, userTgId: String) {
        val reviews = service.findUserReviews(userTgId)
        if (reviews.isEmpty())
            execute(reaction.sendText(tgId, "Отзывов пока нет"))
        reviews.forEach {
            execute(reaction.sendText(tgId, it))
        }
    }

    private suspend fun processReviewUser(mainUser: TgBotUsers, tgId: String? = null, review: UserToUserReview? = null) {
        if (tgId != null) {
            val user = service.findUser(tgId) ?: return
            service.saveUserReview(
                UserToUserReview(
                    userId = user.id,
                    fromUserId = mainUser.id
                ),
                true
            )
            execute(reaction.sendText(mainUser.userDetails.tgId!!, "Оставьте отзыв на пользователя ${user.userDetails.nickName}"))
        } else {
            service.saveUserReview(
                review!!,
                false
            )
            execute(reaction.sendText(mainUser.userDetails.tgId!!, "Спасибо, Ваш отзыв сохранен!"))
            mainScreen(mainUser.userDetails.tgId!!, mainUser)
        }
    }

    private suspend fun processMakeRate(tgId: String, msg: String, user: TgBotUsers) {
        val s = msg.replace("make_rate_", "").split("_")
        service.saveRate(tgId, s[1], s[0].toInt())
        execute(reaction.sendText(tgId, "Спасибо, Ваш рейтинг оставлен!"))
        mainScreen(tgId, user)
    }

    private fun processRateUser(tgId: String, id: String) {
        execute(reaction.sendText(
            tgId,
            "Оцените пользователя",
            listOf(
                ButtonItem(
                    name = "1",
                    action = "make_rate_1_$id"
                ),
                ButtonItem(
                    name = "2",
                    action = "make_rate_2_$id"
                ),
                ButtonItem(
                    name = "3",
                    action = "make_rate_3_$id"
                ),
                ButtonItem(
                    name = "4",
                    action = "make_rate_4_$id"
                ),
                ButtonItem(
                    name = "5",
                    action = "make_rate_5_$id"
                )
            )
        ))
    }

    private suspend fun sendNews(
        msg: String,
        type: UserType? = null,
        update: Update? = null,
        isMarketing: Boolean? = null,
        nickName: String? = null,
        tgId: String? = null
    ) {
        var users = service.getAllUsers()
        if (type != null) {
            users = users.filter { it.userDetails.type == type }
        }
        if (users.isEmpty())
            return
        var file: File? = null
        if (update != null && !update.message.photo.isNullOrEmpty()) {
            val getFile = GetFile()
            getFile.fileId = update.message.photo[update.message.photo.size - 1].fileId
            file = downloadFile(execute(getFile).filePath)
        }
        val marketing = isMarketing ?: false
        users.forEach {
            try {
                if (marketing) {
                    if (nickName.isNullOrEmpty() || nickName.contains("null"))
                        execute(reaction.sendText(it.userDetails.tgId!!, "‼\uFE0FРЕКЛАМА‼\uFE0F"))
                    else {
                        execute(reaction.sendText(it.userDetails.tgId!!, "‼\uFE0FРЕКЛАМА от $nickName‼\uFE0F", listOf(
                            ButtonItem(
                                name = "Карточка покупателя",
                                action = "get_seller_card_$tgId"
                            )
                        )))
                    }
                }
                if (file != null)
                    execute(reaction.sendPhoto(it.userDetails.tgId!!, file = file))
                execute(reaction.sendText(it.userDetails.tgId!!, msg))
            } catch (e: Exception) {
                logger.error("Error sending to ${it.userDetails.tgId!!}", e)
            }
        }
    }

    private suspend fun processSupportMessage(
        supportMessage: SupportMessages?,
        msg: String,
        tgId: String
    ) {
        if (supportMessage == null)
            return
        supportMessage.messageDetails.message = msg
        service.updateSupportMessage(supportMessage)
        execute(reaction.sendScreen(tgId, service.getScreen(Screen.SUPPORT_MESSAGES_CREATE)))
    }

    private suspend fun processDeleteAccount(tgId: String) {
        service.deleteAccount(tgId)
        service.deleteSupportMessage(tgId)
        execute(reaction.sendScreen(tgId, service.getScreen(Screen.DELETED_ACCOUNT)))
    }

    private suspend fun processBuyout(buyout: OwnerBuyouts?, tgId: String, msg: String) {
        if (buyout == null)
            return
        var screen: Screen? = null
        if (buyout.buyoutDetails.link == null) {
            buyout.buyoutDetails.link = msg
            if (msg.contains(WB, ignoreCase = true) || msg.contains(LAMODA, ignoreCase = true)) {
                val parseType = if (msg.contains(WB, ignoreCase = true))
                    WB
                else
                    LAMODA
                val parseDto = try {
                    parser[parseType]?.parse(msg)
                } catch (e: Exception) {
                    logger.error("Error: ", e)
                    null
                }
                buyout.buyoutDetails.buyoutDescription = parseDto?.description
                if (parseDto?.productImages != null)
                    buyout.buyoutDetails.productImages = parseDto.productImages?.map { Image(it) }
                buyout.buyoutDetails.article = parseDto?.articul
                buyout.buyoutDetails.productPrice = parseDto?.price
                buyout.buyoutDetails.name = parseDto?.name ?: ""

                buyout.buyoutDetails.market = if (msg.contains(WB, ignoreCase = true))
                        WB
                    else if (msg.contains(LAMODA, ignoreCase = true))
                        LAMODA
                    else if (msg.contains(OZON, ignoreCase = true))
                        OZON
                    else
                        null
            }
            service.updateBuyout(buyout)
            screen = if (buyout.buyoutDetails.name.isEmpty())
                Screen.SELLER_PRODUCT_ADD_NAME
            else if (buyout.buyoutDetails.startDate == null)
                Screen.SELLER_PRODUCT_ADD_START_DATE_INPUT
            else if (buyout.buyoutDetails.endDate == null)
                Screen.SELLER_PRODUCT_ADD_START_DATE_CONFIRM_END_DATE_INPUT
            else if (buyout.buyoutDetails.instruction == null)
                Screen.SELLER_PRODUCT_DESCRIPTION_TZ
            else
                Screen.SELLER_PRODUCT_ADD_END_DATE_CONFIRM_COUNT_INPUT

        } else if (buyout.buyoutDetails.name.isEmpty()) {
            buyout.buyoutDetails.name = msg
            service.updateBuyout(buyout)
            screen = Screen.SELLER_PRODUCT_ADD_DESCRIPTION
        } else if (buyout.buyoutDetails.buyoutDescription.isNullOrEmpty()) {
            buyout.buyoutDetails.buyoutDescription = msg
            service.updateBuyout(buyout)
            screen = Screen.SELLER_PRODUCT_ADD_START_DATE_INPUT
        } else if (buyout.buyoutDetails.startDate == null) {
            buyout.buyoutDetails.startDate = strToDate(msg)
            service.updateBuyout(buyout)
            screen = Screen.SELLER_PRODUCT_ADD_START_DATE_CONFIRM_END_DATE_INPUT
        } else if (buyout.buyoutDetails.endDate == null) {
            buyout.buyoutDetails.endDate = strToDate(msg)
            if (buyout.buyoutDetails.startDate!!.isAfter(buyout.buyoutDetails.endDate))
                buyout.buyoutDetails.endDate = buyout.buyoutDetails.endDate!!.plusYears(1)
            service.updateBuyout(buyout)
            screen = Screen.SELLER_PRODUCT_DESCRIPTION_TZ
        } else if (buyout.buyoutDetails.instruction == null) {
            buyout.buyoutDetails.instruction = msg
            service.updateBuyout(buyout)
            screen = Screen.SELLER_PRODUCT_ADD_END_DATE_CONFIRM_COUNT_INPUT
        } else if (buyout.buyoutDetails.countPerDay == null) {
            buyout.buyoutDetails.countPerDay = msg.toInt()
            buyout.buyoutDetails.status = BuyoutStatus.ACTIVE
            service.updateBuyout(buyout)
            sendNotifyToBuyer(buyout)
        }
        if (screen != null) {
            service.deleteSupportMessage(tgId)
            execute(reaction.sendScreen(tgId, service.getScreen(screen)))
            return
        }
        processGoodCard(tgId, buyout.id.toString())
    }

    private suspend fun sendNotifyToBuyer(buyout: OwnerBuyouts) {
        doAsync(mdcContext = MDCContext()) {
            val buyers = service.getBuyers()
            if (!buyers.isNullOrEmpty()) {
                buyers.forEach {
                    try {
                        execute(
                            reaction.sendText(
                                it.userDetails.tgId!!,
                                "*Добавлен новый товар:*\n${buyout.buyoutDetails.name}",
                                listOf(
                                    ButtonItem(
                                        name = "Просмотреть товар",
                                        action = "$SELECT_GOOD${buyout.id}"
                                    )
                                )
                            )
                        )
                    } catch (e: Exception) {
                        logger.error("Error sending to user ${it.userDetails.tgId} : ", e)
                    }
                }
            }
        }
    }

    private suspend fun processGetGood(tgId: String, id: String) {
        service.deleteSupportMessage(tgId)
        val buyout = service.savaClientBuyout(tgId, UUID.fromString(id))
        if (buyout == null) {
            execute(
                reaction.sendText(
                    tgId,
                    "Вы уже оставляли заявку на выкуп данного товара или данный товар был удален"
                )
            )
        } else {
            execute(
                reaction.sendText(
                    tgId,
                    "Заявка на выкуп товара оставлена!\nПродавец свяжется с вами в ближайшее время"
                )
            )
            val buttons = mutableListOf(
                ButtonItem(
                    name = "Карточка покупателя",
                    action = "get_seller_card_$tgId"
                )
            )
            if (service.activeSubscription(buyout.clientDetails.buyerTgId)) {
                buttons.add(
                    ButtonItem(
                        name = "Связаться с покупателем",
                        url = "$TG_LINK${service.findUser(tgId)?.userDetails?.getTgNickName()}"
                    )
                )
                buttons.add(
                    ButtonItem(
                        name = "Оценить покупателя",
                        action = "rate_buyer_${buyout.clientDetails.buyerTgId}"
                    )
                )
                buttons.add(
                    ButtonItem(
                        name = if (service.reviewExists(tgId, buyout.clientDetails.buyerTgId)) "Обновить отзыв на покупателя" else "Оставить отзыв на покупателя",
                        action = "review_buyer_${buyout.clientDetails.buyerTgId}"
                    )
                )
            }
            buttons.add(
                ButtonItem(
                    name = "Отклонить выкуп",
                    action = "declined_my_buyout_$id"
                )
            )
            buttons.add(
                ButtonItem(
                    name = "Выкуп состоялся",
                    action = "done_my_buyout_$id"
                )
            )
            execute(reaction.sendText(
                buyout.clientDetails.buyerTgId,
                buyout.clientDetails.name,
                buttons
            ))
        }
    }

    private suspend fun processDeleteGood(tgId: String, id: String) {
        service.deleteGood(id)
        getMyGoods(tgId)
    }

    private suspend fun processSelectSeller(tgId: String, id: String) {
        val user = service.findUser(id)
        service.updateInstagramFollowers(user)
        var message = "*Продавец:* ${user?.userDetails?.getMaskedNickName()}\n" +
                "*Дата регистрации:* ${user?.createdAt?.formatToString()}\n" +
                "*Последняя активность:* ${user?.updatedAt?.formatToString()}\n"
        val buttons = mutableListOf<ButtonItem>()
        if (service.activeSubscription(tgId)) {
            val activeBuyouts = service.getActiveBuyouts(id).size
            message += "*Кол-во активных товаров:* $activeBuyouts\n"
            val clientBuyouts = service.getClientBuyoutsForSeller(id, UserType.SELLER).size
            message += "*Кол-во выкупов:* $clientBuyouts\n"
            buttons.add(
                ButtonItem(
                    name = "Связаться с продавцом",
                    url = "$TG_LINK${user?.userDetails?.getTgNickName()}"
                )
            )
            val rate = service.getRate(user?.id!!)
            message += "*Рейтинг:* $rate \n"
            message += "*Смена ника:* ${user.userDetails.nickNameChangeCount ?: 0}\n"
            val isSubscribed = if (service.activeSubscription(id)) "Активна ⭐" else "Неактивна ❌"
            message += "\n*Подписка*: $isSubscribed"

            buttons.add(
                ButtonItem(
                    name = "Оценить продавца",
                    action = "rate_seller_${user.userDetails.tgId}"
                )
            )
            buttons.add(
                ButtonItem(
                    name = if (service.reviewExists(tgId, user.userDetails.tgId!!))
                        "Обновить отзыв на продавца"
                    else
                        "Оставить отзыв на продавца",
                    action = "review_buyer_${user.userDetails.tgId}"
                )
            )
            buttons.add(
                ButtonItem(
                    name = "Отзывы на продавца",
                    action = "list_review_buyer_${user.userDetails.tgId}"
                )
            )
        }
        execute(reaction.sendText(tgId, message, buttons))
    }

    private suspend fun processSelectBuyer(tgId: String, id: String) {
        val user = service.findUser(id)
        service.updateInstagramFollowers(user)
        val buyouts = service.getClientBuyoutsForSeller(id, UserType.BUYER)
        val rate = service.getRate(user?.id!!)
        var message = "*Покупатель:* ${user.userDetails.getTgNickName()}\n" +
                "*Инстаграм:* ${user.userDetails.getInstagramLink()}\n" +
                "*Дата регистрации:* ${user.createdAt.formatToString()}\n" +
                "*Последняя активность:* ${user.updatedAt.formatToString()}\n"
        val buttons = mutableListOf<ButtonItem>()
        if (service.activeSubscription(tgId) || tgId == id) {
            message += "*Кол-во подписчиков:* ${user.userDetails.followers ?: 0}\n" +
                    "*Кол-во подписок:* ${user.userDetails.following ?: 0}\n" +
                    "*Кол-во выкупов:* ${buyouts.size}\n" +
                    "*Рейтинг:* $rate \n" +
                    "*Смена ника:* ${user.userDetails.nickNameChangeCount ?: 0}"
            val isSubscribed = if (service.activeSubscription(user.userDetails.tgId!!)) "Активна ⭐" else "Неактивна ❌"
            message += "\n*Подписка*: $isSubscribed"
            if (user.userDetails.instruction != null)
                message += "\n\n*Инструкция для сотрудничества:*\n${user.userDetails.instruction}"
            if (tgId == id) {
                buttons.add(
                    ButtonItem(
                        name = "Изменить инстаграм",
                        action = "change_my_instagram"
                    )
                )
                buttons.add(
                    ButtonItem(
                        name = "Инструкция по сотрудничеству",
                        action = "buyer_instruction_create"
                    )
                )
            } else {
                buttons.add(
                    ButtonItem(
                        name = "Связаться с покупателем",
                        url = "$TG_LINK${user.userDetails.getTgNickName()}"
                    )
                )
                buttons.add(
                    ButtonItem(
                        name = "Оценить покупателя",
                        action = "rate_buyer_${user.userDetails.tgId}"
                    )
                )
                buttons.add(
                    ButtonItem(
                        name = if (service.reviewExists(
                                tgId,
                                user.userDetails.tgId!!
                            )
                        ) "Обновить отзыв на покупателя" else "Оставить отзыв на покупателя",
                        action = "review_buyer_${user.userDetails.tgId}"
                    )
                )
                buttons.add(
                    ButtonItem(
                        name = "Отзывы на покупателя",
                        action = "list_review_buyer_${user.userDetails.tgId}"
                    )
                )
            }
        }
        execute(reaction.sendText(tgId, message, buttons))
    }

    private suspend fun processGoodCard(tgId: String, id: String) {
        val good = service.selectGood(id)
        val user = service.findUser(good.buyoutDetails.tgId)
        val instruction = if (good.buyoutDetails.instruction.isNullOrEmpty())
                DEFAULT_INSTRUCTION.format(user?.userDetails?.nickName)
            else
                good.buyoutDetails.instruction
        var goodMessage = "*${good.buyoutDetails.name}*\n" +
                "*Инструкция по выкупу*: $instruction\n" +
                "*Дата начала*: ${good.buyoutDetails.startDate!!.formatToString()}\n" +
                "*Дата окончания*: ${good.buyoutDetails.endDate!!.formatToString()}\n"
        if (good.buyoutDetails.productPrice != null)
            goodMessage += "*Цена:* ${good.buyoutDetails.productPrice}\n"
        if (good.buyoutDetails.countPerDay != null)
            goodMessage += "*Кол-во доступных выкупов в сутки:* ${good.buyoutDetails.countPerDay}\n"
        if (good.buyoutDetails.article != null && good.buyoutDetails.tgId == tgId)
            goodMessage += "*Артикул:* ${good.buyoutDetails.article}\n"
        if (good.buyoutDetails.link != null) {
            if (good.buyoutDetails.tgId == tgId)
                goodMessage += "*Ссылка на товар:* ${good.buyoutDetails.link}\n"
            else if (service.activeSubscription(tgId))
                goodMessage += "*Ссылка на товар:* ${good.buyoutDetails.link}\n"
        }
        if (good.buyoutDetails.market != null)
            goodMessage += "*Витрина:* ${good.buyoutDetails.market}\n"
        if (good.buyoutDetails.buyoutVideo != null)
            execute(reaction.sendVideo(tgId, good.buyoutDetails.buyoutVideo!!))
        if (!good.buyoutDetails.productImages.isNullOrEmpty()) {
            try {
                execute(reaction.sendPhoto(tgId, good.buyoutDetails.productImages!![0].url))
            } catch (e: Exception) {
                logger.error("Error: ", e)
            }
        }
        if (good.buyoutDetails.buyoutDescription != null)
            goodMessage += "\n*Описание*: ${good.buyoutDetails.buyoutDescription}"

        service.deleteSupportMessage(tgId)
        val buttons = if (tgId == good.buyoutDetails.tgId)
            listOf(ButtonItem("Удалить товар", "$DELETE_GOOD${good.id}"))
        else {
            listOf(
                ButtonItem("Оставить заявку на выкуп товара", "$GET_GOOD${good.id}")
            )
        }.toMutableList()
        if (tgId != good.buyoutDetails.tgId && service.activeSubscription(tgId)) {
            buttons.add(
                ButtonItem(
                    name = "Карточка продавца",
                    action = "get_buyer_card_${good.buyoutDetails.tgId}"
                )
            )
            buttons.add(
                ButtonItem("Написать продавцу", url = "$TG_LINK${service.findUser(good.buyoutDetails.tgId)?.userDetails?.getTgNickName()}")
            )
            buttons.add(
                ButtonItem("Все товары продавца", action = "all_seller_goods_${good.buyoutDetails.tgId}")
            )
            val isSubscribed = if (service.activeSubscription(good.buyoutDetails.tgId)) "Активна ⭐" else "Неактивна ❌"
            goodMessage += "\n*Подписка:* $isSubscribed"
        }
        execute(reaction.sendText(
            tgId,
            goodMessage,
            buttons
        ))
    }

    private suspend fun processCommands(msg: String, user: TgBotUsers) {
        service.deleteSupportMessage(user.userDetails.tgId!!)
        when (msg) {
            Commands.FAQ -> {
                val faq = service.faq(user.userDetails.type)
                val faqText = StringBuilder()
                faq.forEach {
                    faqText.append("*${it.faqDetails.name}*")
                        .append("\n")
                        .append(it.faqDetails.description)
                        .append("\n\n")
                }
                execute(reaction.sendText(user.userDetails.tgId!!, faqText.toString()))
            }
            Commands.START -> mainScreen(user.userDetails.tgId!!, user)
            Commands.DELETE -> execute(reaction.sendScreen(user.userDetails.tgId!!, service.getScreen(Screen.DELETE_ACCOUNT)))
            Commands.INSTRUCTION -> execute(reaction.sendScreen(user.userDetails.tgId!!, service.getScreen(Screen.INSTRUCTION_SCREEN)))
            Commands.SUPPORT -> {
                service.createSupportMessage(user.userDetails.tgId!!)
                execute(reaction.sendScreen(user.userDetails.tgId!!, service.getScreen(Screen.SUPPORT_MESSAGES)))
            }
            Commands.SUBSCRIBE -> {
                if (user.userDetails.subscribeEndDate == null || user.userDetails.subscribeEndDate!!.isBefore(LocalDateTime.now())) {
                    val action = if (user.userDetails.subscribeEndDate == null) "Для оформления подписки" else "Ваша подписка завершена ${user.userDetails.subscribeEndDate!!.formatToString()}\nДля продления подписки"
                    if (user.userDetails.subscribeId == null)
                        service.setSubscribeId(user)
                    val cost = if (user.userDetails.type == UserType.SELLER) 500 else 250
                    execute(reaction.sendPhoto(
                        user.userDetails.tgId!!,
                        "https://minio-be-happy.samoletgroup.ru:9000/00nfg3aptz27zouisekb/1.png"
                    ))
                    execute(reaction.sendText(user.userDetails.tgId!!,
                        "$action:\n\n*1.* Отсканируйте *QR код*\n\n" +
                                "*2.* Введите сумму $cost рублей\n\n" +
                                "*3.* В назначении платежа укажите *${user.userDetails.subscribeId}*\n\n" +
                                "*4.* Пришлите чек перевода @Weedowar\n\n" +
                                "или\n\n" +
                                "*1.* Войдет в любой удобный для Вас онлайн-банк\n\n" +
                                "*2.* Выберите перевод по номеру телефона\n\n" +
                                "*3.* Введите номер *+79265269678*\n\n" +
                                "*4.* Выберите Тинькофф банк\n\n" +
                                "*5.* Введите сумму $cost рублей\n\n" +
                                "*6.* В назначении платежа укажите *${user.userDetails.subscribeId}*\n\n" +
                                "*7.* Пришлите чек перевода @Weedowar\n\n" +
                                "После подтверждения оплаты, подписка станет доступна в течении 2х минут"
                        )
                    )
                } else {
                    execute(reaction.sendText(user.userDetails.tgId!!, "Ваша подписка активна до: ${user.userDetails.subscribeEndDate!!.formatToString()}"))
                }
            }
        }
    }

    private suspend fun processButtons(update: Update, userId: String, msg: String) {
        service.deleteSupportMessage(userId)
        when (msg) {
            Buttons.REGISTER_SELLER -> {
                service.getRegistrationScreen(msg, getLogin(update), userId)
                mainScreen(userId, service.findUser(userId)!!)
            }
            Buttons.REGISTER_BUYER -> {
                execute(reaction.sendScreen(
                    userId,
                    service.getRegistrationScreen(msg, getLogin(update), userId)
                ))
            }
            Buttons.ADD_BUYOUT -> startAddBuyout(userId)
            Buttons.MY_GOODS -> getMyGoods(userId)
            Buttons.GET_AVAILABLE_BUYOUTS -> getTopGoods(userId)
            Buttons.SEARCH_AVAILABLE_BUYOUTS -> startSearch(userId)
            Buttons.MY_BUYOUT -> getMyBuyouts(userId, UserType.SELLER)
            Buttons.SELLER_INSTRUCTION -> execute(reaction.sendVideo(userId, videoSellerUrl))
            Buttons.SELLER_INSTRUCTION_SUBSCRIBE -> execute(reaction.sendVideo(userId, videoSellerSubscribeUrl))
            Buttons.BUYER_INSTRUCTION -> execute(reaction.sendVideo(userId, videoBuyerUrl))
            Buttons.BUYER_INSTRUCTION_SUBSCRIBE -> execute(reaction.sendVideo(userId, videoBuyerSubscribeUrl))
            Buttons.GET_MY_CLIENT_BUYOUTS -> getMyBuyouts(userId, UserType.BUYER)
            Buttons.CREATE_SUBSCRIBE -> execute(reaction.sendScreen(userId, service.getScreen(Screen.CREATE_SUBSCRIPTION)))
            Buttons.SEARCH_ALL_SUBS -> createSubscription(userId)
            Buttons.DELETE_ALL_SUBS -> deletedSubscription(userId)
            Buttons.MY_REVIEW -> {
                val reviews = service.getUserReviews(userId)
                if (reviews.isEmpty())
                    execute(reaction.sendText(userId, "Отзывов пока нет"))
                else
                    reviews.forEach {
                        execute(reaction.sendText(userId, it))
                    }
            }
            Buttons.BUYER_INSTRUCTION_CREATE -> {
                service.updateUserLastAction(userId, "${Buttons.BUYER_INSTRUCTION_CREATE}_start")
                execute(reaction.sendText(userId, "Введите инструкцию по сотрудничеству для продавца\nНапример:\n" +
                        "\uD83D\uDD25ПРОДАЮЩАЯ РЕКЛАМА ВАШЕГО ТОВАРА С WB и OZON\uD83D\uDD25\n" +
                        "\n" +
                        "‼\uFE0F1+1 - при покупке рекламы на один товар, реклама второго - БЕСПЛАТНО‼\uFE0F\n" +
                        "\n" +
                        "\uD83D\uDED1Присылайте ссылку на товар сразу\uD83D\uDED1\n" +
                        "\n" +
                        "‼\uFE0FБАРТЕР рассматриваем‼\uFE0F\n" +
                        "\nhttps://instagram.com/your_account\n"))
            }
            Buttons.BUYER_NEWS_CREATE -> {
                val news = service.getLastNews(userId)
                if (news != null) {
                    val days = ChronoUnit.DAYS.between(
                        news.createdAt,
                        LocalDateTime.now()
                    )
                    if (days < 7L)
                        execute(reaction.sendText(userId, "Вы уже отправляли новость на этой неделе :(\nВ следующий раз новость можно будет отправить ${news.createdAt.plusDays(7).formatToString()}"))
                } else {
                    service.updateUserLastAction(userId, "buyer_news_create_start")
                    execute(reaction.sendText(userId, """                      
                        Напишите новость, о которой должны узнать все :)
                        
                        Небольшая инструкция:
                        - Новость будет опубликована мгновенно и отправлена всем продавцам на площадке. 🏎🏎🏎
                        
                        - Всего доступно для отправки 4 новости в месяц по одной каждую неделю. 4️⃣
                        
                        - Рекомендуем предварительно отформатировать новость в личных сообщениях (Избранное), чтобы понимать, как она будет выглядеть в рассылке. ℹ️
                        
                        - Новостью может быть рекламное предложение о сотрудничестве, условиях бартера, либо любое другое рекламное предложение не нарушающее правило морали, не несущее негативные эмоции и сомнительные предложения, которые могут быть расценены администрацией бота как пирамида, инфоцыганство и прочее мошенничество. ⚠️
                        ‼️За потенциальные попытки мошенничества или оскорбления возможна блокировка учетной записи в боте без возможности восстановления.‼️
                         
                    """.trimIndent()))
                }
            }
            Buttons.TOP_BLOGGERS -> execute(reaction.sendText(userId, "Список топ блогеров", service.findTopBloggers(0)))
            Buttons.CREATE_MARKETING -> execute(reaction.sendText(
                userId,
                "По вопросам сотрудничества, размещения рекламы и продвижения обращайтесь к @Weedowar",
                listOf(
                    ButtonItem(
                        name = "Связаться с администратором @Weedowar",
                        url = "https://t.me/Weedowar"
                    ))
            ))
        }
    }

    private suspend fun deletedSubscription(userId: String) {
        service.deleteSubscription(userId)
        execute(reaction.sendScreen(userId, service.getScreen(Screen.DELETED_SUBSCRIPTION)))
        mainScreen(userId, service.findUser(userId)!!)
    }
    private suspend fun createSubscription(userId: String, searchValue: String? = null) {
        service.createSubscription(userId, searchValue)
        execute(reaction.sendScreen(userId, service.getScreen(Screen.CREATED_SUBSCRIPTION)))
        mainScreen(userId, service.findUser(userId)!!)
    }

    private suspend fun startSearch(userId: String) {
        service.deleteSupportMessage(userId)
        execute(reaction.sendScreen(userId, service.startSearch(userId)))
    }

    private suspend fun endSearch(userId: String, msg: String?) {
        getGoodsScreen(userId, service.endSearch(userId, msg))
    }

    private suspend fun getGoodsScreen(userId: String, goods: List<OwnerBuyouts>) {
        service.deleteSupportMessage(userId)
        if (goods.isEmpty()) {
            execute(reaction.sendText(userId, "Товары не найдены"))
            return
        }
        execute(
            reaction.sendText(userId,
                "Список товаров",
                goods.map {
                    ButtonItem(name = it.buyoutDetails.name, action = "$SELECT_GOOD${it.id}")
                }
            )
        )
    }

    private suspend fun getTopGoods(userId: String) {
        getGoodsScreen(userId, service.getTopBuyouts(userId))
    }

    private suspend fun getMyGoods(userId: String) {
        getGoodsScreen(userId, service.getActiveBuyouts(userId))
    }

    private suspend fun getOwnerGoods(userId: String, ownerId: String) {
        getGoodsScreen(userId, service.getActiveBuyouts(ownerId))
    }

    private suspend fun getMyBuyouts(userId: String, type: UserType) {
        service.deleteSupportMessage(userId)
        val list = service.getClientBuyoutsForSeller(userId, type)
        if (list.isEmpty()) {
            execute(reaction.sendText(userId, "У вас пока нет выкупов"))
        }
        else {
            list.forEach {
                execute(reaction.sendText(
                    userId, it.clientDetails.name,
                    createButtons(type,
                            if (type == UserType.SELLER)
                                it.clientDetails.tgId
                            else
                                it.clientDetails.buyerTgId,
                            userId,
                            it.id
                        )
                    )
                )
            }
        }
    }

    private suspend fun createButtons(type: UserType, tgId: String, userId: String, id: UUID? = null): List<ButtonItem> {
        val list = mutableListOf<ButtonItem>()
        if (type == UserType.SELLER) {
            list.add(
                ButtonItem(
                    name = "Карточка покупателя",
                    action = "get_seller_card_$tgId"
                )
            )
        }
        if (type == UserType.BUYER) {
            list.add(
                ButtonItem(
                    name = "Карточка продавца",
                    action = "get_buyer_card_$tgId"
                )
            )
        }
        if (service.activeSubscription(userId)) {
            if (type == UserType.SELLER) {
                list.add(
                    ButtonItem(
                        name = "Связаться с покупателем",
                        url = "$TG_LINK${service.findUser(tgId)?.userDetails?.getTgNickName()}"
                    )
                )
            }
            list.add(
                ButtonItem(
                    name = if (type == UserType.SELLER)
                        "Связаться с покупателем"
                    else
                        "Связаться с продавцом",
                    url = "$TG_LINK${service.findUser(tgId)?.userDetails?.getTgNickName()}"
                )
            )
            list.add(
                ButtonItem(
                    name = if (type == UserType.SELLER)
                        "Оценить покупателя"
                    else
                        "Оценить продавца",
                    action = if (type == UserType.SELLER)
                        "rate_buyer_$tgId"
                    else
                        "rate_seller_$tgId"
                )
            )
            val reviewExists = if (service.reviewExists(userId, tgId)) "Обновить" else "Оставить"
            list.add(
                ButtonItem(
                    name = if (type == UserType.SELLER)
                        "$reviewExists отзыв на покупателя"
                    else
                        "$reviewExists отзыв на продавца",
                    action = if (type == UserType.SELLER)
                        "review_buyer_$tgId"
                    else
                        "review_seller_$tgId"
                )
            )
        }
        if (id != null) {
            list.add(
                ButtonItem(
                    name = "Отклонить выкуп",
                    action = "declined_my_buyout_$id"
                )
            )
            list.add(
                ButtonItem(
                    name = "Выкуп состоялся",
                    action = "done_my_buyout_$id"
                )
            )
        }
        return list.toList()
    }

    private suspend fun startAddBuyout(userId: String) {
        service.deleteSupportMessage(userId)
        service.createDraftBuyout(userId)
        execute(reaction.sendScreen(userId, service.getScreen(Screen.SELLER_PRODUCT_ADD_LINK)))
    }

    private suspend fun processRegistration(userId: String, user: TgBotUsers, msg: String, status: UserStatus) {
        when (status) {
            UserStatus.NEED_INSTAGRAM -> {
                service.saveInstagram(msg, user)
                mainScreen(userId, user)
            }
            else -> service.saveInstagram(msg, user)
        }
    }

    private fun getLogin(update: Update): String =
        "@${(update.message?.from ?: update.callbackQuery?.from)?.userName}"

    private suspend fun mainScreen(userId: String, user: TgBotUsers) {
        service.updateUserLastAction(userId, "/start")
        service.deleteSupportMessage(userId)
        val screen = service.getScreen(
            if (user.userDetails.type == UserType.SELLER) Screen.SELLER_CHOOSE_ACTION else Screen.BUYER_CHOOSE_ACTION
        )
        if (service.activeSubscription(userId) &&
            screen.buttons != null) {
            if (user.userDetails.type == UserType.BUYER) {
                screen.buttons?.addItem(
                    ButtonItem(
                        name = "Инструкция по сотрудничеству",
                        action = "buyer_instruction_create"
                    )
                )
                screen.buttons?.addItem(
                    ButtonItem(
                        name = "Опубликовать новость",
                        action = "buyer_news_create"
                    )
                )
                screen.buttons?.addItem(
                    ButtonItem(
                        name = "Автопоиск товаров",
                        action = "create_subscribe"
                    )
                )
                screen.buttons?.addItem(
                    ButtonItem(
                        name = "Моя карточка",
                        action = "my_card"
                    )
                )
            } else {
                screen.buttons?.addItem(
                    ButtonItem(
                        name = "Топ блогеров",
                        action = "top_bloggers"
                    )
                )
            }
            screen.buttons?.addItem(
                ButtonItem(
                    name = "❗Разместить рекламу❗",
                    action = "create_marketing"
                )
            )
        }
        execute(reaction.sendScreen(userId, screen))
    }

    private suspend fun getUserName(chatId: String) {
        try {
            val chat = GetChat()
            chat.chatId = chatId
            service.updateUserName(chatId, execute(chat).userName)
        } catch (e: Exception) {
            logger.error("Error getting chat:", e)
        }
    }
    @Scheduled(cron = "0 0 15 * * MON")
    fun callNickNameUpdate() {
        logger.info("Start update nickName at: {}", LocalDateTime.now())
        doLaunch {
            service.getAllUsers().filter { it.userDetails.status != UserStatus.BLOCKED }.chunked(50)
                .forEach {
                    chunk ->
                    run {
                        chunk.forEach {
                            getUserName(it.userDetails.tgId!!)
                        }
                    }
                }
        }
        logger.info("End update nickName at: {}", LocalDateTime.now())
    }

    @Scheduled(cron = "0 */30 * ? * *")
    fun sendInform() {
        doLaunch {
            service.findAllForInform().forEach {
                execute(reaction.sendText(
                    it.tgId,
                    "*Ваша подписка успешно активирована*\n" +
                            "*Дата окончания подписки:* ${it.subscribeEndDate.formatToString()}\n" +
                            "*ID подписки:* ${it.subscribeId}\n\n" +
                            "Если у вас возникнут вопросы, обращайтесь в поддержку @Weedowar\n" +
                            "и не забудь сообщить Ваш ID подписки"
                ))
            }
        }
    }

    @Scheduled(cron = "0 */1 * ? * *")
    fun checkSubscribeEnd() {
        doLaunch {
            val list = service.getAllUsers()
            if (list.isNotEmpty()) {
                list.filter {
                    it.userDetails.subscribeId != null &&
                            it.userDetails.subscribeEndDate != null &&
                            getDateDiff(it.userDetails.subscribeEndDate!!, LocalDateTime.now()) >= 1
                }.forEach {
                    execute(reaction.sendText(
                        it.userDetails.tgId!!,
                        "Ваша подписка подходит к концу, дата завершения ${it.userDetails.subscribeEndDate!!.formatToString()}\n"
                    ))
                }
            }
        }
    }

    private fun getDateDiff(st: LocalDateTime, ed: LocalDateTime): Long =
        ChronoUnit.DAYS.between(
            st,
            ed
        )
}
