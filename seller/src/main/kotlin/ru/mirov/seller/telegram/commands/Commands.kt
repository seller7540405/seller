package ru.mirov.seller.telegram.commands

object Commands {
    const val FAQ = "/faq"
    const val START = "/start"
    const val DELETE = "/delete"
    const val INSTRUCTION = "/instr"
    const val SUPPORT = "/support"
    const val SUBSCRIBE = "/subs"
}
