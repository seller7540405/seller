package ru.mirov.seller.telegram.reaction.impl

import org.springframework.stereotype.Component
import org.telegram.telegrambots.meta.api.methods.ActionType
import org.telegram.telegrambots.meta.api.methods.send.*
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessages
import org.telegram.telegrambots.meta.api.objects.InputFile
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow
import ru.mirov.seller.database.model.ButtonItem
import ru.mirov.seller.database.model.ButtonType
import ru.mirov.seller.database.model.TgScreens
import ru.mirov.seller.extensions.logger.Loggable
import ru.mirov.seller.integration.photo.MediaGate
import ru.mirov.seller.telegram.reaction.BotReaction
import ru.mirov.seller.utils.markdown
import java.io.File

@Component
class BotReactionImpl (
    private val mediaGate: MediaGate
): BotReaction {
    companion object: Loggable
    override fun sendChatAction(
        chatId: String,
        action: ActionType
    ): SendChatAction {
        val chatAction = SendChatAction()
        chatAction.chatId = chatId
        chatAction.setAction(action)
        return chatAction
    }

    override fun sendScreen(chatId: String, screens: TgScreens): SendMessage {
        if (screens.buttons == null)
            return sendMessage(chatId, screens.title)
        val screen = when (screens.buttons!!.type) {
            ButtonType.ROW -> sendButtons(chatId, screens.title, screens.buttons!!.items)
            ButtonType.REPLY -> sendMenu(chatId, screens.title, screens.buttons!!.items)
            else -> sendMessage(chatId, screens.title, screens.buttons!!.items)
        }
        return screen
    }

    override fun sendText(chatId: String, message: String, buttons: List<ButtonItem>?): SendMessage =
        if (buttons.isNullOrEmpty()) sendMessage(chatId, message) else sendButtons(chatId, message, buttons)

    override suspend fun sendVideo(
        chatId: String,
        video: String
    ): SendVideo {
        val message = SendVideo()
        val videoFile = InputFile()
        videoFile.setMedia(mediaGate.download(video), "1.mp4")
        message.video = videoFile
        message.chatId = chatId
        return message
    }

    override suspend fun sendPhoto(chatId: String, photo: String?, file: File?): SendPhoto {
        val message = SendPhoto()
        val image = InputFile()
        if (photo != null)
            image.setMedia(mediaGate.download(photo), "1.png")
        else if (file != null)
            image.setMedia(file)
        message.photo = image
        message.chatId = chatId
        return message
    }

    override suspend fun deleteMessage(chatId: String, messageId: List<Int>): DeleteMessages? = try {
        DeleteMessages(chatId, messageId)
    } catch (e: Exception) {
        logger.info("Error: ", e)
        null
    }

    private fun sendButtons(
        chatId: String,
        text: String,
        buttons: List<ButtonItem>
    ): SendMessage {
        val message = SendMessage()
        message.text = text.markdown()
        message.chatId = chatId
        message.replyMarkup = getButtons(buttons)
        message.parseMode = "MarkdownV2"
        return message
    }

    private fun getButtons(buttons: List<ButtonItem>): InlineKeyboardMarkup {
        val inlineKeyboardMarkup = InlineKeyboardMarkup();
        val rowList = mutableListOf<List<InlineKeyboardButton>>();
        buttons.forEach {
            logger.info("Button {}", it)
            val inlineKeyboardButton = InlineKeyboardButton()
            inlineKeyboardButton.text = it.name
            inlineKeyboardButton.callbackData = it.action
            inlineKeyboardButton.url = it.url
            val keyboardButtonsRow: MutableList<InlineKeyboardButton> = mutableListOf()
            keyboardButtonsRow.add(inlineKeyboardButton)
            rowList.add(keyboardButtonsRow)
        }
        inlineKeyboardMarkup.keyboard = rowList;
        return inlineKeyboardMarkup
    }

    private fun sendMessage(
        chatId: String,
        text: String,
        buttons: List<ButtonItem>? = null
    ): SendMessage {
        val message = SendMessage()
        message.text = text.markdown()
        message.chatId = chatId
        if (!buttons.isNullOrEmpty()) {
            val inlines: MutableList<InlineKeyboardButton> = mutableListOf()
            buttons.forEach {
                val goButton = InlineKeyboardButton()
                goButton.callbackData = it.action
                goButton.text = it.name
                inlines.add(goButton)
            }
            message.replyMarkup = InlineKeyboardMarkup(mutableListOf(inlines))
        }
        message.parseMode = "MarkdownV2"
        return message
    }

    private fun sendMenu(chatId: String, title: String, buttons: List<ButtonItem>): SendMessage {
        val message = SendMessage()
        message.text = title.markdown()
        message.chatId = chatId
        message.parseMode = "MarkdownV2"
        val inlines: MutableList<KeyboardRow> = mutableListOf()
        buttons.forEach {
            val goButton = KeyboardRow()
            goButton.add(it.name)
            inlines.add(goButton)
        }
        message.replyMarkup = ReplyKeyboardMarkup(inlines.toList())
        return message
    }
}
