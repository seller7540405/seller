package ru.mirov.seller.telegram.actions

object Actions {
    const val REGISTRATION = "REGISTRATION"
    const val SEARCH = "SEARCH"
    const val SILENT = "SILENT"

    val ACTIONS: Set<String> = setOf(
        "buyer_instruction_create_start",
        "buyer_news_create_start",
        "change_my_instagram"
    )
}
