package ru.mirov.seller.telegram.reaction

import org.telegram.telegrambots.meta.api.methods.ActionType
import org.telegram.telegrambots.meta.api.methods.send.SendChatAction
import org.telegram.telegrambots.meta.api.methods.send.SendMessage
import org.telegram.telegrambots.meta.api.methods.send.SendPhoto
import org.telegram.telegrambots.meta.api.methods.send.SendVideo
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessages
import ru.mirov.seller.database.model.ButtonItem
import ru.mirov.seller.database.model.TgScreens
import java.io.File

interface BotReaction {
    fun sendChatAction(
        chatId: String,
        action: ActionType
    ): SendChatAction

    fun sendScreen(
        chatId: String,
        screens: TgScreens
    ): SendMessage

    fun sendText(
        chatId: String,
        message: String,
        buttons: List<ButtonItem>? = null
    ): SendMessage

    suspend fun sendVideo(
        chatId: String,
        video: String
    ): SendVideo

    suspend fun sendPhoto(
        chatId: String,
        photo: String? = null,
        file: File? = null
    ): SendPhoto

    suspend fun deleteMessage(
        chatId: String,
        messageId: List<Int>
    ): DeleteMessages?
}
