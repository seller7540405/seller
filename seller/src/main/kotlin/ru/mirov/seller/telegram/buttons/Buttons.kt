package ru.mirov.seller.telegram.buttons

object Buttons {
    const val REGISTER_SELLER = "register_seller"
    const val REGISTER_BUYER = "register_buyer"
    const val ADD_BUYOUT = "add_buyout"
    const val MY_GOODS = "my_goods"
    const val GET_AVAILABLE_BUYOUTS = "get_available_buyouts"
    const val SEARCH_AVAILABLE_BUYOUTS = "search_available_buyouts"
    const val MY_BUYOUT = "my_buyout"
    const val SELLER_INSTRUCTION = "seller_instruction"
    const val BUYER_INSTRUCTION = "buyer_instruction"
    const val GET_MY_CLIENT_BUYOUTS = "get_my_client_buyouts"
    const val CREATE_SUBSCRIBE = "create_subscribe"
    const val SEARCH_ALL_SUBS = "search_all_subs"
    const val DELETE_ALL_SUBS = "delete_all_subs"
    const val MY_REVIEW = "my_review"
    const val BUYER_INSTRUCTION_CREATE = "buyer_instruction_create"
    const val BUYER_NEWS_CREATE = "buyer_news_create"
    const val TOP_BLOGGERS = "top_bloggers"
    const val CREATE_MARKETING = "create_marketing"
    const val SELLER_INSTRUCTION_SUBSCRIBE = "seller_instruction_subscribe"
    const val BUYER_INSTRUCTION_SUBSCRIBE = "buyer_instruction_subscribe"

    val BUTTONS: Set<String> = setOf(
        REGISTER_SELLER,
        REGISTER_BUYER,
        ADD_BUYOUT,
        MY_GOODS,
        GET_AVAILABLE_BUYOUTS,
        SEARCH_AVAILABLE_BUYOUTS,
        MY_BUYOUT,
        SELLER_INSTRUCTION,
        BUYER_INSTRUCTION,
        GET_MY_CLIENT_BUYOUTS,
        CREATE_SUBSCRIBE,
        SEARCH_ALL_SUBS,
        DELETE_ALL_SUBS,
        MY_REVIEW,
        BUYER_INSTRUCTION_CREATE,
        BUYER_NEWS_CREATE,
        TOP_BLOGGERS,
        CREATE_MARKETING,
        SELLER_INSTRUCTION_SUBSCRIBE,
        BUYER_INSTRUCTION_SUBSCRIBE
    )
}
