package ru.mirov.seller.integration.photo

import java.io.InputStream

interface MediaGate {
    suspend fun download(file: String): InputStream?
}
