package ru.mirov.seller.integration.config

import io.netty.channel.ChannelOption
import io.netty.handler.timeout.ReadTimeoutHandler
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.client.reactive.ReactorClientHttpConnector
import org.springframework.http.codec.ClientCodecConfigurer
import org.springframework.web.reactive.function.client.ClientRequest
import org.springframework.web.reactive.function.client.ExchangeFilterFunction
import org.springframework.web.reactive.function.client.ExchangeStrategies
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono
import reactor.netty.Connection
import reactor.netty.http.client.HttpClient
import java.util.concurrent.TimeUnit
import java.util.function.Consumer

@Configuration
class WebClientConfig {
    companion object {
        private var log = LoggerFactory.getLogger(WebClientConfig::class.java)

        private const val CONNECT_TIMEOUT_MILLIS = 180000
        private const val READ_TIMEOUT_MILLIS = 180000

        private fun logRequest(): ExchangeFilterFunction =
            ExchangeFilterFunction.ofRequestProcessor { clientRequest: ClientRequest ->
                log.info("Request: {} {}", clientRequest.method(), clientRequest.url())
                clientRequest.headers().forEach { name: String?, values: List<String?> ->
                    values.forEach(Consumer { value: String? -> log.info("{}={}", name, value) })
                }
                Mono.just(clientRequest)
            }
    }

    @Bean
    fun webClient(): WebClient {
        val size = 32 * 2048 * 2048
        val strategies = ExchangeStrategies.builder()
            .codecs { codecs: ClientCodecConfigurer -> codecs.defaultCodecs().maxInMemorySize(size) }
            .build()
        return WebClient
            .builder()
            .filter(logRequest())
            .exchangeStrategies(strategies)
            .clientConnector(
                ReactorClientHttpConnector(
                    HttpClient.create()
                        .httpResponseDecoder { spec -> spec.maxHeaderSize(size) }
                        .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, CONNECT_TIMEOUT_MILLIS)
                        .doOnConnected { connection: Connection ->
                            connection.addHandlerFirst(ReadTimeoutHandler(
                                READ_TIMEOUT_MILLIS.toLong(), TimeUnit.MILLISECONDS)) })).build()
    }
}
