package ru.mirov.seller.integration.photo.impl

import kotlinx.coroutines.reactor.awaitSingle
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import ru.mirov.seller.integration.photo.MediaGate
import java.io.InputStream

@Component
class MediaGateImpl(
    private val webClient: WebClient
): MediaGate {
    override suspend fun download(file: String): InputStream? =
        try {
            webClient
                .get()
                .uri(file)
                .retrieve()
                .bodyToMono(ByteArray::class.java)
                .awaitSingle().inputStream()
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
}
