package ru.mirov.seller.database.model

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import ru.mirov.seller.database.config.json.StoreJson

@Table("TG_SCREENS")
class TgScreens (
    @Id
    @Column("ID")
    val id: String,
    @Column("TITLE")
    val title: String,
    @Column("BUTTONS")
    var buttons: Button? = null
)

@StoreJson
data class Button (
    val type: ButtonType,
    var items: List<ButtonItem>
) {
    @JsonIgnore
    fun addItem(item: ButtonItem) {
        val items = this.items.toMutableList()
        items.add(item)
        this.items = items.toList()
    }
}

data class ButtonItem (
    val name: String,
    var action: String? = null,
    var url: String? = null
)

enum class ButtonType {
    ROW,
    COLUMN,
    REPLY
}
