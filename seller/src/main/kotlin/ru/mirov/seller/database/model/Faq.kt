package ru.mirov.seller.database.model

import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import ru.mirov.seller.database.config.json.StoreJson
import ru.mirov.seller.database.config.entity.AbstractEntity
import java.time.LocalDateTime
import java.util.*

@Table("FAQ")
class Faq (
    id: UUID? = null,
    createdAt: LocalDateTime = LocalDateTime.now(),
    updatedAt: LocalDateTime = LocalDateTime.now(),
    @Column("FAQ_DETAILS")
    val faqDetails: FaqDetail
): AbstractEntity<UUID>(id, createdAt, updatedAt) {
    companion object {
        const val ENTITY_NANE = "FAQ_ENTITY"
    }
}

@StoreJson
data class FaqDetail (
    val name: String,
    val status: ActionStatus,
    val description: String,
    val type: FaqType
)

enum class FaqStatus {
    ACTIVE,
    CLOSED
}

enum class FaqType {
    FOR_BUYER,
    FOR_SELLER
}


