package ru.mirov.seller.database.repository

import kotlinx.coroutines.flow.Flow
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository
import ru.mirov.seller.database.model.BuyerSubscription
import java.util.UUID

@Repository
interface BuyerSubscriptionRepository: CoroutineCrudRepository<BuyerSubscription, UUID> {
    @Query("select * from BUYER_SUBSCRIPTION")
    fun findAllSubscriptions(): Flow<BuyerSubscription>

    @Query("select * from BUYER_SUBSCRIPTION where TG_ID = :tgId limit 1")
    fun findByTgId(tgId: String): Flow<BuyerSubscription>

    @Query("select * from BUYER_SUBSCRIPTION where TG_ID = :tgId")
    fun findAllByTgId(tgId: String): Flow<BuyerSubscription>
}
