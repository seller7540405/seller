package ru.mirov.seller.database.model

import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import ru.mirov.seller.database.config.json.StoreJson
import ru.mirov.seller.database.config.entity.AbstractEntity
import java.time.LocalDateTime
import java.util.*

@Table("BUYOUT_ACTIONS")
class BuyoutActions (
    id: UUID? = null,
    createdAt: LocalDateTime = LocalDateTime.now(),
    updatedAt: LocalDateTime = LocalDateTime.now(),
    @Column("ACTION_DETAILS")
    val actionDetails: AppealDetail
): AbstractEntity<UUID>(id, createdAt, updatedAt) {
    companion object {
        const val ENTITY_NANE = "BUYOUT_ACTIONS_ENTITY"
    }
}

@StoreJson
data class AppealDetail (
    val name: String,
    val status: ActionStatus,
    val description: String,
    val tgId: String,
    val buyoutId: UUID,
    val type: ActionType
)

enum class ActionStatus {
    ACTIVE,
    CLOSED
}

enum class ActionType {
    APPEAL,
    PAYMENT
}
