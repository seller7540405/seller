package ru.mirov.seller.database.config.isolation

import org.springframework.transaction.TransactionDefinition.*

enum class IsolationLevel(val isolationLevel: Int) {
    DEFAULT(ISOLATION_DEFAULT),
    READ_COMMITTED(ISOLATION_READ_COMMITTED),
    REPEATABLE_READ(ISOLATION_REPEATABLE_READ),
    SERIALIZABLE(ISOLATION_SERIALIZABLE)
}
