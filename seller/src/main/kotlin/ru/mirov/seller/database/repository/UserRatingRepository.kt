package ru.mirov.seller.database.repository

import kotlinx.coroutines.flow.Flow
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository
import ru.mirov.seller.database.model.UserRating
import java.util.UUID

@Repository
interface UserRatingRepository: CoroutineCrudRepository<UserRating, UUID> {
    fun findByRatedUserId(ratedUserId: UUID): Flow<UserRating>
}
