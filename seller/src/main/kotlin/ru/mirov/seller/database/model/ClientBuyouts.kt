package ru.mirov.seller.database.model

import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import ru.mirov.seller.database.config.json.StoreJson
import ru.mirov.seller.database.config.entity.AbstractEntity
import java.time.LocalDateTime
import java.util.*

@Table("CLIENT_BUYOUTS")
class ClientBuyouts (
    id: UUID? = null,
    createdAt: LocalDateTime = LocalDateTime.now(),
    updatedAt: LocalDateTime = LocalDateTime.now(),
    @Column("CLIENT_DETAILS")
    val clientDetails: ClientDetail
): AbstractEntity<UUID>(id, createdAt, updatedAt) {
    companion object {
        const val ENTITY_NANE = "CLIENT_BUYOUTS_ENTITY"
    }
}

@StoreJson
data class ClientDetail (
    val name: String,
    var status: ClientBuyoutStatus,
    val buyoutId: UUID,
    val tgId: String,
    val buyerTgId: String,
    var orderImageProofs: List<Image>? = null,
    var buyingImageProofs: List<Image>? = null,
    var isOrderProofedBySeller: Boolean? = null,
    var sellerOrderProofCheckingDescription: String? = null,
    var reviewImageProofs: List<Image>? = null,
    var isBuyingProofedBySeller: Boolean? = null
)

enum class ClientBuyoutStatus {
    RESERVED,
    ORDERED,
    ORDER_PROOF_WAITING,
    ORDER_PROOFED,
    BUYING_PROOFS_ATTACHED,
    REVIEW_PROOFS_ATTACHED,
    APPROVED,
    DECLINED
}
