package ru.mirov.seller.database.repository

import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository
import ru.mirov.seller.database.model.TgScreens

@Repository
interface TgScreensRepository: CoroutineCrudRepository<TgScreens, String>
