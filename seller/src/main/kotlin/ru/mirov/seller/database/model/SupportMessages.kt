package ru.mirov.seller.database.model

import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import ru.mirov.seller.database.config.entity.AbstractEntity
import ru.mirov.seller.database.config.json.StoreJson
import java.time.LocalDateTime
import java.util.*

@Table("SUPPORT_MESSAGES")
class SupportMessages (
    id: UUID? = null,
    createdAt: LocalDateTime = LocalDateTime.now(),
    updatedAt: LocalDateTime = LocalDateTime.now(),
    @Column("MESSAGE_DETAIL")
    val messageDetails: MessageDetails
): AbstractEntity<UUID>(id, createdAt, updatedAt) {
    companion object {
        const val ENTITY_NANE = "TG_BOT_USERS_ENTITY"
    }
}

@StoreJson
data class MessageDetails (
    var tgId: String? = null,
    var message: String? = null
)
