package ru.mirov.seller.database.repository

import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository
import ru.mirov.seller.database.model.BuyoutActions
import java.util.UUID

@Repository
interface BuyoutActionsRepository: CoroutineCrudRepository<BuyoutActions, UUID>
