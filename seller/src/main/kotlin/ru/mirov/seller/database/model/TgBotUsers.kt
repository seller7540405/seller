package ru.mirov.seller.database.model

import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import ru.mirov.seller.database.config.json.StoreJson
import ru.mirov.seller.database.config.entity.AbstractEntity
import java.time.LocalDateTime
import java.util.*
import kotlin.collections.List

@Table("TG_BOT_USERS")
class TgBotUsers (
    id: UUID? = null,
    createdAt: LocalDateTime = LocalDateTime.now(),
    updatedAt: LocalDateTime = LocalDateTime.now(),
    @Column("USER_DETAILS")
    val userDetails: UserDetails
): AbstractEntity<UUID>(id, createdAt, updatedAt) {
    companion object {
        const val ENTITY_NANE = "TG_BOT_USERS_ENTITY"
    }
}

@StoreJson
data class UserDetails (
    var tgId: String? = null,
    var lastAction: String? = null,
    var status: UserStatus? = null,
    var name: String? = null,
    var nickName: String? = null,
    val type: UserType,
    var email: String? = null,
    var phone: String? = null,
    var inn: String? = null,
    var socialLink: List<SocialLink>? = null,
    var byOutsCount: Int? = null,
    var followers: Int? = null,
    var following: Int? = null,
    var subscribeId: String? = null,
    var subscribeStartDate: LocalDateTime? = null,
    var subscribeEndDate: LocalDateTime? = null,
    var instruction: String? = null,
    var nickNameChangeCount: Int? = 0,
    var instRequestDate: LocalDateTime? = null
) {
    @JsonIgnore
    fun getTgNickName() = this.nickName?.replace("@", "")

    @JsonIgnore
    fun getInstagramLink(): String {
        var link = this.socialLink?.get(0)?.link ?: return ""
        if (link.startsWith("@"))
            link = link.replace("@", "")
        if (!link.startsWith("https://www.instagram.com/"))
            link = "https://www.instagram.com/$link"
        return link
    }

    @JsonIgnore
    fun getMaskedNickName(): String {
        val nick = this.nickName?.replace("@", "") ?: return ""
        return "\\*\\*\\*${nick.substring(3)}"
    }
}

enum class UserType {
    SELLER,
    BUYER
}

data class SocialLink (
    val name: SocialType,
    val link: String
)

enum class UserStatus {
    NEED_PHONE,
    NEED_EMAIL,
    NEED_INN,
    CONFIRMED,
    NEED_INSTAGRAM,
    BLOCKED
}

enum class SocialType {
    INSTAGRAM
}
