package ru.mirov.seller.database.repository

import kotlinx.coroutines.flow.Flow
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository
import ru.mirov.seller.database.model.TgBotUsers
import java.util.UUID

@Repository
interface TgBotUsersRepository: CoroutineCrudRepository<TgBotUsers, UUID> {
    @Query("select * from TG_BOT_USERS where USER_DETAILS ->> 'tgId' = :tgId")
    fun findByTgId(tgId: String): Flow<TgBotUsers>
    @Query("select * from TG_BOT_USERS where USER_DETAILS ->> 'type' = 'BUYER'")
    fun findActiveBuyers(): Flow<TgBotUsers>
    @Query("select * from TG_BOT_USERS where USER_DETAILS ->> 'subscribeId' = :subscribeId")
    fun findBySubscriptionId(subscribeId: String): Flow<TgBotUsers>

    @Query("""
        select *
          from tg_bot_users tbu 
         where user_details ->> 'type' = 'BUYER' 
           and user_details ->> 'following' is not null
           and user_details ->> 'followers' is not null
           and user_details ->> 'nickName' is not null
      order by cast(user_details ->> 'followers' as bigint) desc,  
               cast(user_details ->> 'following' as bigint) desc
      LIMIT :limit OFFSET :offset
    """)
    suspend fun findTopBloggers(
        offset: Int,
        limit: Int
    ): List<TgBotUsers>
}
