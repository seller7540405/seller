package ru.mirov.seller.database.config.json

import com.fasterxml.jackson.databind.ObjectMapper
import io.r2dbc.postgresql.codec.Json
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.core.convert.TypeDescriptor
import org.springframework.core.convert.converter.GenericConverter
import org.springframework.core.type.filter.AnnotationTypeFilter
import org.springframework.data.convert.ReadingConverter
import org.springframework.data.convert.WritingConverter
import org.springframework.data.r2dbc.convert.R2dbcCustomConversions
import org.springframework.data.r2dbc.dialect.PostgresDialect

@Configuration(proxyBeanMethods = false)
class StoreJsonConfig {
    @Bean
    @Primary
    fun jsonConverters(
        objectMapper: ObjectMapper
    ): R2dbcCustomConversions = R2dbcCustomConversions.of(
        PostgresDialect.INSTANCE,
        ClassPathScanningCandidateComponentProvider(false)
            .apply {
                addIncludeFilter(AnnotationTypeFilter(StoreJson::class.java))
            }
            .findCandidateComponents("ru.mirov")
            .asSequence()
            .map { Class.forName(it.beanClassName) }
            .map { clz ->
                listOf(
                    @WritingConverter
                    object : GenericConverter {
                        override fun getConvertibleTypes() = setOf(GenericConverter.ConvertiblePair(clz, Json::class.java))
                        override fun convert(source: Any?, p1: TypeDescriptor, p2: TypeDescriptor) =
                            Json.of(objectMapper.writeValueAsString(source))
                    },
                    @ReadingConverter
                    object : GenericConverter {
                        override fun getConvertibleTypes() = setOf(GenericConverter.ConvertiblePair(Json::class.java, clz))
                        override fun convert(source: Any?, p1: TypeDescriptor, p2: TypeDescriptor) =
                            objectMapper.readValue((source as Json).asString(), clz)
                    }
                )
            }
            .flatten()
            .plus(
                listOf(
                    @WritingConverter
                    object : GenericConverter {
                        override fun getConvertibleTypes() = setOf(GenericConverter.ConvertiblePair(Json::class.java, Json::class.java))
                        override fun convert(source: Any?, p1: TypeDescriptor, p2: TypeDescriptor) = source
                    },
                    @ReadingConverter
                    object : GenericConverter {
                        override fun getConvertibleTypes() = setOf(GenericConverter.ConvertiblePair(Json::class.java, Json::class.java))
                        override fun convert(source: Any?, p1: TypeDescriptor, p2: TypeDescriptor) = source
                    }
                )
            )
            .toList()
    )
}
