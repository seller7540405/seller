package ru.mirov.seller.database.repository

import kotlinx.coroutines.flow.Flow
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository
import ru.mirov.seller.database.model.SupportMessages
import java.util.UUID

@Repository
interface SupportMessagesRepository: CoroutineCrudRepository<SupportMessages, UUID> {
    @Query("select * from SUPPORT_MESSAGES where MESSAGE_DETAIL ->> 'tgId' = :tgId")
    fun findByTgId(tgId: String): Flow<SupportMessages>
}
