package ru.mirov.seller.database.model

import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import ru.mirov.seller.database.config.entity.AbstractEntity
import java.time.LocalDateTime
import java.util.*

@Table("SUBSCRIBE_INFORM")
class SubscribeInform (
    id: UUID? = null,
    createdAt: LocalDateTime = LocalDateTime.now(),
    updatedAt: LocalDateTime = LocalDateTime.now(),
    @Column("IS_SEND")
    var isSend: Boolean = false,
    @Column("TG_ID")
    var tgId: String,
    @Column("SUBSCRIBE_ID")
    var subscribeId: String,
    @Column("SUBSCRIBE_START_DATE")
    var subscribeStartDate: LocalDateTime,
    @Column("SUBSCRIBE_END_DATE")
    var subscribeEndDate: LocalDateTime,
): AbstractEntity<UUID>(id, createdAt, updatedAt) {
    companion object {
        const val ENTITY_NANE = "SUBSCRIBE_INFORM_ENTITY"
    }
}
