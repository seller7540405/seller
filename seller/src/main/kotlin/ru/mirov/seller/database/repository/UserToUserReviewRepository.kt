package ru.mirov.seller.database.repository

import kotlinx.coroutines.flow.Flow
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository
import ru.mirov.seller.database.model.UserToUserReview
import java.util.UUID

@Repository
interface UserToUserReviewRepository: CoroutineCrudRepository<UserToUserReview, UUID> {
    fun findByUserIdAndReviewIsNotNullOrderByCreatedAtDesc(userId: UUID): Flow<UserToUserReview>
    fun findByUserIdAndFromUserId(userId: UUID, fromUserId: UUID): Flow<UserToUserReview>
    fun findByFromUserIdAndReviewIsNull(fromUserId: UUID): Flow<UserToUserReview>
    fun findByUserIdAndReviewIsNotNull(userId: UUID): Flow<UserToUserReview>
}
