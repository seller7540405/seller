package ru.mirov.seller.database.config.isolation

import org.springframework.transaction.ReactiveTransactionManager
import org.springframework.transaction.TransactionDefinition
import org.springframework.transaction.reactive.TransactionalOperator
import org.springframework.transaction.reactive.executeAndAwait

interface TransactionManager {
    suspend fun <T> execute(isolationLevel: IsolationLevel = IsolationLevel.DEFAULT, action: suspend () -> T?): T?

    suspend fun <T> executeReadOnly(action: suspend () -> T?): T?
}

class TransactionManagerImpl(reactiveTransactionManager: ReactiveTransactionManager) : TransactionManager {

    companion object {
        private val readOnlyDefinition = object : TransactionDefinition {
            override fun isReadOnly() = true
        }
    }

    private val isolationLevelOperators = IsolationLevel.entries.associateWith {
        TransactionalOperator.create(reactiveTransactionManager,
            object : TransactionDefinition {
                override fun getIsolationLevel() = it.isolationLevel
            }
        )
    }

    private val readOnlyTransactionalOperator = TransactionalOperator.create(reactiveTransactionManager, readOnlyDefinition)

    override suspend fun <T> execute(isolationLevel: IsolationLevel, action: suspend () -> T?): T? {
        return isolationLevelOperators[isolationLevel]!!.executeAndAwait { action() }
    }

    override suspend fun <T> executeReadOnly(action: suspend () -> T?): T? {
        return readOnlyTransactionalOperator.executeAndAwait { action() }
    }
}
