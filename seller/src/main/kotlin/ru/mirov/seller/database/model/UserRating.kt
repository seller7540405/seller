package ru.mirov.seller.database.model

import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import ru.mirov.seller.database.config.entity.AbstractEntity
import java.time.LocalDateTime
import java.util.*

@Table("USER_RATING")
class UserRating (
    id: UUID? = null,
    createdAt: LocalDateTime = LocalDateTime.now(),
    updatedAt: LocalDateTime = LocalDateTime.now(),
    @Column("RATING")
    val rating: Int,
    @Column("USER_ID")
    val userId: UUID,
    @Column("RATED_USER_ID")
    val ratedUserId: UUID
): AbstractEntity<UUID>(id, createdAt, updatedAt) {
    companion object {
        const val ENTITY_NANE = "USER_RATING_ENTITY"
    }
}
