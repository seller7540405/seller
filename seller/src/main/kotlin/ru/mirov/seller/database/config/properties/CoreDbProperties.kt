package ru.mirov.seller.database.config.properties

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties("application.core.db")
data class CoreDbProperties(
    val storeJsonBasePackage: String = "ru.mirov",
    val throwExceptionIfIdNull: Boolean = true
)
