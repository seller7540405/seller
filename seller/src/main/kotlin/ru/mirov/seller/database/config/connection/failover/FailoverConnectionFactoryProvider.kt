package ru.mirov.seller.database.config.connection.failover

import io.r2dbc.spi.ConnectionFactories
import io.r2dbc.spi.ConnectionFactory
import io.r2dbc.spi.ConnectionFactoryOptions
import io.r2dbc.spi.ConnectionFactoryProvider
import org.springframework.stereotype.Component
import java.time.Duration

@Component
class FailoverConnectionFactoryProvider : ConnectionFactoryProvider {

    companion object {
        private const val FAILOVER_DRIVER = "failover"
        private const val COLON = ':'
        private const val COMMA = ','

        private fun toDuration(value: Any?): Duration? = when (value) {
            null -> null
            is Duration -> value
            is String -> Duration.parse(value.toString())
            else -> throw IllegalArgumentException("Указан не корректный тип $value для конвертации в Duration")
        }
    }

    override fun create(connectionFactoryOptions: ConnectionFactoryOptions): ConnectionFactory {
        val protocol = connectionFactoryOptions.getRequiredValue(ConnectionFactoryOptions.PROTOCOL).toString()
        if (protocol.isBlank()) {
            throw IllegalArgumentException("Не указан протокол для создания коннекта к БД")
        }

        val protocols = protocol.trim().split(COLON, limit = 2).toTypedArray()
        val driverDelegate = protocols[0]

        val protocolDelegate = if (protocols.size == 2) protocols[1] else ""

        val host = connectionFactoryOptions.getRequiredValue(ConnectionFactoryOptions.HOST).toString()
        if (host.isBlank()) {
            throw IllegalArgumentException("Не указан хост для подключения к БД")
        }

        val hosts = host.trim().split(COMMA)
        val connectionFactories: MutableList<ConnectionFactory> = ArrayList(hosts.size)

        hosts.forEach { hostDelegate ->
            val hostAndPort = hostDelegate.split(COLON, limit = 2)
            val portDelegate = if (hostAndPort.size == 2) hostAndPort[1].toInt() else null

            val newOptions = ConnectionFactoryOptions.builder()
                .from(connectionFactoryOptions)
                .option(ConnectionFactoryOptions.DRIVER, driverDelegate)
                .option(ConnectionFactoryOptions.PROTOCOL, protocolDelegate)
                .option(ConnectionFactoryOptions.HOST, hostAndPort[0])
            if (portDelegate != null) {
                newOptions.option(ConnectionFactoryOptions.PORT, portDelegate)
            }

            val connectionFactory = ConnectionFactories.find(newOptions.build())
                ?: throw IllegalArgumentException("Не удалось найти указанный driver подключения к БД $driverDelegate")
            connectionFactories.add(connectionFactory)
        }

        if (connectionFactories.isEmpty()) {
            throw IllegalArgumentException("Не указаны urls подключения к БД")
        }

        if (connectionFactories.size == 1) {
            return connectionFactories[0]
        }

        return FailoverConnectionFactory.builder()
            .addConnectionFactories(connectionFactories)
            .let { builder ->
                val timeout = toDuration(connectionFactoryOptions.getValue(ConnectionFactoryOptions.CONNECT_TIMEOUT))
                if (timeout != null) {
                    builder.connectTimeout(timeout)
                } else {
                    builder
                }
            }
            .build()
    }

    override fun supports(connectionFactoryOptions: ConnectionFactoryOptions): Boolean =
        driver == (connectionFactoryOptions.getValue(ConnectionFactoryOptions.DRIVER)?.toString() ?: "")

    override fun getDriver() = FAILOVER_DRIVER

}
