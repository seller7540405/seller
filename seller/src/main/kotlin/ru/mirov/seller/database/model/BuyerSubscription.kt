package ru.mirov.seller.database.model

import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import ru.mirov.seller.database.config.entity.AbstractEntity
import java.time.LocalDateTime
import java.util.*

@Table("BUYER_SUBSCRIPTION")
class BuyerSubscription (
    id: UUID? = null,
    createdAt: LocalDateTime = LocalDateTime.now(),
    updatedAt: LocalDateTime = LocalDateTime.now(),
    @Column("TG_ID")
    val tgId: String,
    @Column("SEARCH_VALUE")
    var searchValue: String? = null
): AbstractEntity<UUID>(id, createdAt, updatedAt) {
    companion object {
        const val ENTITY_NANE = "BUYER_SUBSCRIPTION_ENTITY"
    }
}
