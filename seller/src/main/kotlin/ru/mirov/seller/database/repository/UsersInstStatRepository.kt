package ru.mirov.seller.database.repository

import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository
import ru.mirov.seller.database.model.UsersInstStat
import java.util.UUID

@Repository
interface UsersInstStatRepository: CoroutineCrudRepository<UsersInstStat, UUID>
