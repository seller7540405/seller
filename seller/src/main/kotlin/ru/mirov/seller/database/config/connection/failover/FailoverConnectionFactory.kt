package ru.mirov.seller.database.config.connection.failover

import io.r2dbc.spi.Connection
import io.r2dbc.spi.ConnectionFactory
import reactor.core.Disposable
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import ru.mirov.seller.extensions.logger.Loggable
import java.time.Duration
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.atomic.AtomicReference

class FailoverConnectionFactory
    private constructor(connectionFactories: List<ConnectionFactory>, connectTimeout: Duration) : ConnectionFactory {

    companion object : Loggable {
        private const val MINIMUM_CONNECTION_FACTORIES_SIZE = 2

        fun builder() = Builder()
    }

    private val connectionFactories: List<ConnectionFactory>
    private val currentConnectionFactoryIndex = AtomicInteger(0)
    private val connectTimeout: Duration
    private val create: Mono<Connection>

    override fun create() = create
    override fun getMetadata() = connectionFactories[0].metadata

    init {
        this.connectionFactories = connectionFactories.toList()
        this.connectTimeout = connectTimeout
        create = Mono.defer { createConnection() }.retry((this.connectionFactories.size - 1).toLong())
    }

    private fun createConnection(): Mono<Connection> {
        val index = currentConnectionFactoryIndex.get()
        logDebug { "Попытка создания коннекта к БД с индексом [$index]" }
        val holder = ConnectionHolder()
        return Mono.from(connectionFactories[index].create())
            .cast(Connection::class.java)
            .doOnNext { connection: Connection? -> holder.setConnection(connection) }
            .timeout(connectTimeout)
            .doOnNext { logDebug { "Успешно создали коннект к БД с индексом [$index], информация по коннекту $it" } }
            .doOnError { throwable: Throwable ->
                holder.dispose()
                currentConnectionFactoryIndex.compareAndSet(index, (index + 1) % connectionFactories.size)
                logError(throwable) { "Ошибка создания коннекта к БД с индексом [$index]" }
            }
    }

    class Builder {

        companion object {
            private val DEFAULT_CONNECT_TIMEOUT = Duration.ofSeconds(15)
        }

        private val connectionFactories: MutableList<ConnectionFactory> = ArrayList()

        private var connectTimeout = DEFAULT_CONNECT_TIMEOUT

        fun connectTimeout(connectTimeout: Duration): Builder {
            if (connectTimeout.isNegative) {
                throw IllegalArgumentException("ConnectTimeout для фабрики подключений должен быть >= 0, указанное значение $connectTimeout")
            }
            this.connectTimeout = connectTimeout
            return this
        }

        fun addConnectionFactory(connectionFactory: ConnectionFactory): Builder {
            connectionFactories.add(connectionFactory)
            return this
        }

        fun addConnectionFactories(connectionFactories: Collection<ConnectionFactory>): Builder {
            connectionFactories.forEach { addConnectionFactory(it) }
            return this
        }

        fun build(): FailoverConnectionFactory {
            if (connectionFactories.size < MINIMUM_CONNECTION_FACTORIES_SIZE) {
                throw IllegalArgumentException("Количество ConnectionFactory должно быть >= $MINIMUM_CONNECTION_FACTORIES_SIZE, указанное количество ConnectionFactory ${connectionFactories.size}")
            }
            return FailoverConnectionFactory(connectionFactories, connectTimeout)
        }

    }

    private class ConnectionHolder : Disposable {

        private val disposed = AtomicBoolean(false)
        private val connection = AtomicReference<Connection?>()

        fun setConnection(connection: Connection?) {
            if (!this.connection.compareAndSet(null, connection)) {
                throw IllegalStateException("Подключение к БД $connection уже установлено!")
            }
            if (isDisposed) {
                closeConnection()
            }
        }

        override fun dispose() {
            if (disposed.compareAndSet(false, true)) {
                closeConnection()
            }
        }

        override fun isDisposed() =  disposed.get()

        private fun closeConnection() {
            connection.getAndSet(null)?.close().toMono().subscribe()
        }
    }

}
