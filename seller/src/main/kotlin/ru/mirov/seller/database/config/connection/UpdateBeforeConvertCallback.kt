package ru.mirov.seller.database.config.connection

import org.reactivestreams.Publisher
import org.springframework.data.r2dbc.mapping.event.BeforeConvertCallback
import org.springframework.data.relational.core.sql.SqlIdentifier
import reactor.core.publisher.Mono
import ru.mirov.seller.database.config.IdGeneratorService
import ru.mirov.seller.database.config.entity.AbstractEntity
import ru.mirov.seller.database.config.properties.CoreDbProperties
import ru.mirov.seller.extensions.logger.Loggable
import ru.mirov.seller.extensions.types.getGenericParameterClass
import ru.mirov.seller.extensions.types.getSupperClassPrivateField
import ru.mirov.seller.extensions.types.setSuperClassPrivateField
import java.time.LocalDateTime
import java.util.*

class UpdateBeforeConvertCallback (
    private val coreDbProperties: CoreDbProperties,
    private val idGeneratorService: IdGeneratorService
) : BeforeConvertCallback<Any> {
    companion object : Loggable {
        private const val ID_FIELD_NAME = "id"
    }
    override fun onBeforeConvert(entity: Any, table: SqlIdentifier): Publisher<Any> {
        if (entity !is AbstractEntity<*>) {
            logWarn { "Сущность ${entity.javaClass.simpleName} не наследуется от ${AbstractEntity::class.simpleName}, обновление полей до update в БД не поддерживается" }
            return Mono.just(entity)
        }

        if (entity.getSupperClassPrivateField<Any>(ID_FIELD_NAME) == null) {
            when (val idClass = entity.getGenericParameterClass(0)) {
                String::class.java -> entity.setSuperClassPrivateField(ID_FIELD_NAME, idGeneratorService.generateId())
                UUID::class.java -> entity.setSuperClassPrivateField(ID_FIELD_NAME, idGeneratorService.generateUUID())
                else -> {
                    if (coreDbProperties.throwExceptionIfIdNull) {
                        throw IllegalStateException("Для сущности ${entity.javaClass.simpleName} указан не поддерживаемый class $idClass для генерации id, поддерживаемые типы [String, UUID]")
                    }
                }
            }
        }

        if (!entity.isNew) {
            entity.updatedAt = LocalDateTime.now()
        }

        return Mono.just(entity)
    }
}
