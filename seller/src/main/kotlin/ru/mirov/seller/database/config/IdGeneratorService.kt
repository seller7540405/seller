package ru.mirov.seller.database.config

import com.fasterxml.uuid.Generators
import org.springframework.stereotype.Component
import java.util.*

interface IdGeneratorService {

    fun generateUUID(): UUID

    fun generateId(): String
}

@Component
class IdGeneratorServiceImpl : IdGeneratorService {

    private val generator = Generators.timeBasedEpochGenerator()

    override fun generateUUID() = generator.generate()!!
    override fun generateId() = generateUUID().toString()
}
