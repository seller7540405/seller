package ru.mirov.seller.database.repository

import kotlinx.coroutines.flow.Flow
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository
import ru.mirov.seller.database.model.OwnerBuyouts
import java.util.UUID

@Repository
interface OwnerBuyoutsRepository: CoroutineCrudRepository<OwnerBuyouts, UUID> {
    @Query(
        """
       select * 
         from OWNER_BUYOUTS 
        where BUYOUT_DETAILS ->> 'tgId' = :tgId
          and BUYOUT_DETAILS ->> 'status' = 'ACTIVE'
    """
    )
    fun findAllByUserId(tgId: String): Flow<OwnerBuyouts>

    @Query(
        """
       select *
  from OWNER_BUYOUTS o
 where BUYOUT_DETAILS ->> 'status' = 'ACTIVE'
   and cast(buyout_details ->> 'endDate' as timestamp) > current_timestamp
   and cast(buyout_details ->> 'startDate' as timestamp) <= current_timestamp
   and not exists (
   	select 1
   	  from CLIENT_BUYOUTS c
   	 where c.CLIENT_DETAILS ->> 'buyoutId' = cast(o.id as varchar)
   	   and c.CLIENT_DETAILS ->> 'tgId' = :userId
   )
   and cast(buyout_details ->> 'countPerDay' as bigint) > (
   	select count(*)
   	  from CLIENT_BUYOUTS c
   	 where c.CLIENT_DETAILS ->> 'buyoutId' = cast(o.id as varchar)  
   	   and TO_CHAR(created_at, 'YYYY/MM/DD') = TO_CHAR(current_timestamp , 'YYYY/MM/DD')   	  
   )
 order by random()  
 limit 10
    """
    )
    fun findTop(userId: String): Flow<OwnerBuyouts>

    @Query(
        """
       select * 
         from OWNER_BUYOUTS 
        where BUYOUT_DETAILS ->> 'tgId' = :tgId
          and BUYOUT_DETAILS ->> 'status' = 'WAITING'
        limit 1  
    """
    )
    fun findDraftByUserId(tgId: String): Flow<OwnerBuyouts>

    @Query("""
select *
  from OWNER_BUYOUTS o
 where BUYOUT_DETAILS ->> 'status' = 'ACTIVE'
   and cast(buyout_details ->> 'endDate' as timestamp) > current_timestamp
   and cast(buyout_details ->> 'startDate' as timestamp) <= current_timestamp
   and not exists (
   	select 1
   	  from CLIENT_BUYOUTS c
   	 where c.CLIENT_DETAILS ->> 'buyoutId' = cast(o.id as varchar)
   	   and c.CLIENT_DETAILS ->> 'tgId' = :userId
   )
   and cast(buyout_details ->> 'countPerDay' as bigint) > (
   	select count(*)
   	  from CLIENT_BUYOUTS c
   	 where c.CLIENT_DETAILS ->> 'buyoutId' = cast(o.id as varchar)  
   	   and TO_CHAR(created_at, 'YYYY/MM/DD') = TO_CHAR(current_timestamp , 'YYYY/MM/DD')   	  
   )
 and (:value is null or 
 to_tsvector(buyout_details ->> 'name') @@ to_tsquery(replace(:value::text, ' ', '|')) or
 to_tsvector(buyout_details ->> 'buyoutDescription') @@ to_tsquery(replace(:value::text, ' ', '|'))
 )   
 order by o.created_at desc
 limit :limit           
    """)
    fun findByValue(
        userId: String,
        value: String? = null,
        limit: Int
    ): Flow<OwnerBuyouts>
}
