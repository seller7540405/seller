package ru.mirov.seller.database.model

import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import ru.mirov.seller.database.config.json.StoreJson
import ru.mirov.seller.database.config.entity.AbstractEntity
import java.time.LocalDateTime
import java.util.*

@Table("OWNER_BUYOUTS")
class OwnerBuyouts (
    id: UUID? = null,
    createdAt: LocalDateTime = LocalDateTime.now(),
    updatedAt: LocalDateTime = LocalDateTime.now(),
    @Column("BUYOUT_DETAILS")
    val buyoutDetails: BuyoutDetail
): AbstractEntity<UUID>(id, createdAt, updatedAt) {
    companion object {
        const val ENTITY_NANE = "OWNER_BUYOUTS_ENTITY"
    }
}

@StoreJson
data class BuyoutDetail (
    var name: String,
    var status: BuyoutStatus,
    var buyoutDescription: String? = null,
    var instruction: String? = null,
    var buyoutVideo: String? = null,
    var countPerDay: Int? = null,
    val tgId: String,
    var startDate: LocalDateTime? = null,
    var endDate: LocalDateTime? = null,
    var article: String? = null,
    var productPrice: String? = null,
    var productImages: List<Image>? = null,
    var link: String? = null,
    var market: String? = null
)

data class Image (
    val url: String
)

enum class BuyoutStatus {
    ACTIVE,
    PAUSED,
    INACTIVE,
    WAITING
}
