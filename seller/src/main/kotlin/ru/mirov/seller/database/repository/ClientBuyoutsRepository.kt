package ru.mirov.seller.database.repository

import kotlinx.coroutines.flow.Flow
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository
import ru.mirov.seller.database.model.ClientBuyouts
import java.util.UUID

@Repository
interface ClientBuyoutsRepository: CoroutineCrudRepository<ClientBuyouts, UUID> {
    @Query("""
       select * 
         from CLIENT_BUYOUTS 
        where CLIENT_DETAILS ->> 'tgId' = :tgId
          and CLIENT_DETAILS ->> 'buyoutId' = :buyoutId
    """)
    fun findAllByUserId(tgId: String, buyoutId: String): Flow<ClientBuyouts>

    @Query("""
       select * 
         from CLIENT_BUYOUTS 
        where CLIENT_DETAILS ->> 'buyerTgId' = :tgId
         and CLIENT_DETAILS ->> 'status' not in ('APPROVED', 'DECLINED')
    """)
    fun findAllBySellerId(sellerId: String): Flow<ClientBuyouts>

    @Query("""
       select * 
         from CLIENT_BUYOUTS 
        where CLIENT_DETAILS ->> 'tgId' = :tgId
         and CLIENT_DETAILS ->> 'status' not in ('APPROVED', 'DECLINED')
    """)
    fun findAllByTgId(sellerId: String): Flow<ClientBuyouts>
}
