package ru.mirov.seller.database.repository

import kotlinx.coroutines.flow.Flow
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository
import ru.mirov.seller.database.model.Faq
import java.util.UUID

@Repository
interface FaqRepository: CoroutineCrudRepository<Faq, UUID> {
    @Query("select * from FAQ where FAQ_DETAILS ->> 'type' = :type and FAQ_DETAILS ->> 'status' = 'ACTIVE'")
    fun findByType(type: String): Flow<Faq>
}
