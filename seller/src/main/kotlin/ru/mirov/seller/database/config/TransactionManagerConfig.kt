package ru.mirov.seller.database.config

import io.r2dbc.spi.ConnectionFactory
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.autoconfigure.r2dbc.R2dbcTransactionManagerAutoConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.r2dbc.connection.R2dbcTransactionManager
import org.springframework.transaction.ReactiveTransactionManager
import ru.mirov.seller.database.config.isolation.TransactionManagerImpl


@Configuration(proxyBeanMethods = false)
@Import(R2dbcTransactionManagerAutoConfiguration::class)
class TransactionManagerConfig {

    @Bean
    fun reactiveTransactionManager(
        @Qualifier("connectionFactory")
        connectionFactory: ConnectionFactory
    ): ReactiveTransactionManager = R2dbcTransactionManager(connectionFactory)

    @Bean
    fun transactionManager(reactiveTransactionManager: ReactiveTransactionManager)
            = TransactionManagerImpl(reactiveTransactionManager)
}
