package ru.mirov.seller.database.repository

import kotlinx.coroutines.flow.Flow
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository
import ru.mirov.seller.database.model.SubscribeInform
import java.util.UUID


@Repository
interface SubscribeInformRepository: CoroutineCrudRepository<SubscribeInform, UUID> {
    @Query("select * from SUBSCRIBE_INFORM where IS_SEND = false")
    suspend fun findAllForInform(): List<SubscribeInform>
    fun findByTgId(tgId: String): Flow<SubscribeInform>
}
