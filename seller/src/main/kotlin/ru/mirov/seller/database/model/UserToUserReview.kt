package ru.mirov.seller.database.model

import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import ru.mirov.seller.database.config.entity.AbstractEntity
import java.time.LocalDateTime
import java.util.*

@Table("USER_TO_USER_REVIEW")
class UserToUserReview (
    id: UUID? = null,
    createdAt: LocalDateTime = LocalDateTime.now(),
    updatedAt: LocalDateTime = LocalDateTime.now(),
    @Column("USER_ID")
    val userId: UUID,
    @Column("FROM_USER_ID")
    val fromUserId: UUID,
    @Column("REVIEW")
    var review: String? = null
): AbstractEntity<UUID>(id, createdAt, updatedAt) {
    companion object {
        const val ENTITY_NANE = "USER_TO_USER_REVIEW_ENTITY"
    }
}
