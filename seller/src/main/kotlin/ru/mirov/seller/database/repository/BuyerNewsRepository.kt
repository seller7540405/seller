package ru.mirov.seller.database.repository

import kotlinx.coroutines.flow.Flow
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.stereotype.Repository
import ru.mirov.seller.database.model.BuyerNews
import java.util.UUID

@Repository
interface BuyerNewsRepository: CoroutineCrudRepository<BuyerNews, UUID> {
    @Query("select * from buyer_news where USER_ID = :userId order by created_at desc limit 1")
    fun findNews(userId: UUID): Flow<BuyerNews>
}
