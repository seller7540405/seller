package ru.mirov.seller.database.model

import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import ru.mirov.seller.database.config.entity.AbstractEntity
import java.time.LocalDateTime
import java.util.*

@Table("BUYER_NEWS")
class BuyerNews (
    id: UUID? = null,
    createdAt: LocalDateTime = LocalDateTime.now(),
    updatedAt: LocalDateTime = LocalDateTime.now(),
    @Column("NEWS_MESSAGE")
    val newsMessage: String,
    @Column("IS_SEND")
    val isSend: Boolean,
    @Column("USER_ID")
    val userId: UUID
): AbstractEntity<UUID>(id, createdAt, updatedAt) {
    companion object {
        const val ENTITY_NANE = "BUYER_NEWS_ENTITY"
    }
}
