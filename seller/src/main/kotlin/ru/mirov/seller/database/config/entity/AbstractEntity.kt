package ru.mirov.seller.database.config.entity

import org.springframework.data.annotation.Id
import org.springframework.data.annotation.Transient
import org.springframework.data.domain.Persistable
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.InsertOnlyProperty
import java.time.LocalDateTime

abstract class AbstractEntity<ID_TYPE>(
    @Id
    @Column("ID")
    private var id: ID_TYPE? = null,

    @Column("CREATED_AT")
    @InsertOnlyProperty
    val createdAt: LocalDateTime = LocalDateTime.now(),

    @Column("UPDATED_AT")
    var updatedAt: LocalDateTime = LocalDateTime.now()
) : Persistable<ID_TYPE> {

    @Transient
    private var justCreated = id == null

    override fun isNew() = justCreated
    fun setNew(new: Boolean) { justCreated = new }

    override fun getId() = id!!
}
