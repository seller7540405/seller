package ru.mirov.seller.database.config

import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.mapping.callback.EntityCallback
import ru.mirov.seller.database.config.connection.UpdateBeforeConvertCallback
import ru.mirov.seller.database.config.properties.CoreDbProperties

@Configuration(proxyBeanMethods = false)
@EnableConfigurationProperties(CoreDbProperties::class)
class CoreDbConfig {

    @Bean
    fun updateBeforeConvertCallback(
        coreDbProperties: CoreDbProperties,
        idGeneratorService: IdGeneratorService
    ): EntityCallback<Any> = UpdateBeforeConvertCallback(coreDbProperties, idGeneratorService)
}
