package ru.mirov.seller.instagram

import org.springframework.stereotype.Service
import ru.mirov.seller.extensions.logger.Loggable

@Service
class InstagramService (
    private val client: InstagramClient
) {
    companion object: Loggable {
        private const val TAG_FOLLOWERS = "Followers"
        private const val TAG_FOLLOWING = "Following"
        private const val TAG_CONTENT = "content=\""
    }

    suspend fun call(nick: String):InstagramDto? {
        val account = client.get(nick) ?: return null
        val followers = account.split(TAG_FOLLOWERS)
        if (followers.isEmpty())
            return null
        val followerCount = followers[0].substring(followers[0].lastIndexOf(TAG_CONTENT).plus(9)).replace(",", "").replace("K", "000").trim().toIntOrNull()
        val following = followers[1].split(TAG_FOLLOWING)
        var followingCount: Int? = null
        if (following.isNotEmpty())
            followingCount = following[0].replace(",", "").replace("K", "000").trim().toInt()
        return InstagramDto(
            id = nick,
            followers = followerCount,
            following = followingCount
        )
    }
}

data class InstagramDto (
    val id: String,
    var followers: Int? = null,
    var following: Int? = null
)
