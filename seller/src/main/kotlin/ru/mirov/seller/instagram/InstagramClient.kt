package ru.mirov.seller.instagram

import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.awaitBody


@Component
class InstagramClient (
    private val webClient: WebClient
){
    companion object {
        private const val URL = "https://www.instagram.com/"
    }
    suspend fun get(userName: String): String? =
        try {
            webClient.get()
                .uri("$URL$userName/")
                .retrieve()
                .awaitBody()
        } catch (e: Exception) {
            null
        }
}
