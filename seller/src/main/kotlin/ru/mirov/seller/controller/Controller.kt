package ru.mirov.seller.controller

import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import ru.mirov.seller.instagram.InstagramService
import ru.mirov.seller.service.Service

@RestController
@RequestMapping
class Controller (
    private val service: Service,
    private val instagramService: InstagramService
){
    @GetMapping("instagram")
    suspend fun get(@RequestParam name: String) = service.updateAllBuyers()

    @PostMapping("/subscribe/{id}", consumes = [MediaType.APPLICATION_JSON_VALUE])
    suspend fun update(@PathVariable("id") id: String) = service.updateSubscription(id)

    @GetMapping("inst")
    suspend fun getNickName(@RequestParam name: String) = instagramService.call(name)

    @PostMapping("mass")
    suspend fun massUpdate() = service.massUpdate()

    @PostMapping("inst-update")
    suspend fun instUpdate() {
        service.getAllUsers().forEach {
            service.updateInstagramFollowers(it)
        }
    }
}
