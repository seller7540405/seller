package ru.mirov.seller.parser.impl

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.awaitBody
import ru.mirov.seller.extensions.logger.Loggable
import ru.mirov.seller.parser.MarketPlaceParser
import ru.mirov.seller.parser.dto.*
import kotlin.math.roundToInt

@Component("wildberries")
class WildberriesParserImpl (
    private val webClient: WebClient,
    @Value("\${application.wildberries.detail}") private val detailUrl: String,
    @Value("\${application.wildberries.catalog}") private val catalogUrl: String
): MarketPlaceParser {
    companion object: Loggable {
        private const val IMAGE_URL = "https://basket-%s.wbbasket.ru/vol%s/part%s/%s/images/big/%s.jpg"
        private const val INFO_URL = "https://basket-%s.wbbasket.ru/vol%s/part%s/%s/info/ru/card.json"
    }
    override suspend fun parse(link: String): GoodDto? {
        val id = getItemId(link) ?: return null
        val detail = getDetail(id) ?: return null
        if (detail.data?.products.isNullOrEmpty())
            return null
        val name = detail.data?.products?.get(0)?.name ?: return null
        val supplierId = detail.data?.products?.get(0)?.supplierId
        val res = GoodDto(
            name = name,
            articul = id
        )
        if (supplierId != null) {
            val products = getFullCatalog(supplierId.toString())
            if (products.isNotEmpty()) {
                val product = products.firstOrNull { it.id.toString() == id }
                if (product != null) {
                    if (product.salePriceU != null)
                        res.price = "${product.salePriceU?.div(100)} ₽"
                    val pics = product.pics ?: 0
                    if (pics > 0) {
                        val images = mutableListOf<String>()
                        for(i in 0 until pics) {
                            images.add(getImageUrl(id.toLong(), (i+1).toString()))
                        }
                        if (images.isNotEmpty())
                            res.productImages = images
                    }
                }
            }
        }
        val card = getCard(getCardUrl(id.toLong()))
        res.description = card?.description
        return res
    }

    private suspend fun getDetail(id: String): WildberriesDetailDto? =
        try {
            webClient.get()
                .uri(detailUrl) {
                    u -> u.queryParam("dest", "-1257786").queryParam("nm", id).build()
                }
                .retrieve()
                .awaitBody()
        } catch (e: Exception) {
            logger.error("Error: ", e)
            null
        }

    private suspend fun getFullCatalog(supplierId: String): List<WildberriesCatalogProducts> {
        val data = getCatalog(supplierId, 1)?.data ?: return emptyList()
        if (data.products.isNullOrEmpty())
            return emptyList()
        val list = (data.products ?: mutableListOf())
        try {
            val total = data.total ?: 0
            if (list.size >= total)
                return list
            val pages = total.toDouble().div(list.size.toDouble()).roundToInt()
            if (pages < 1)
                return list
            if (pages <= 2) {
                val newList = getCatalog(supplierId, 2)?.data?.products
                if (!newList.isNullOrEmpty())
                    list.addAll(newList)
                return list
            }
            for (i in 2..pages) {
                val newList = getCatalog(supplierId, i)?.data?.products
                if (!newList.isNullOrEmpty())
                    list.addAll(newList)
            }
            return list
        } catch (e: Exception) {
            return list
        }
    }

    private suspend fun getCatalog(supplierId: String, page: Int): WildberriesCatalogDto? =
        try {
            webClient.get()
                .uri(catalogUrl){
                    u -> u.queryParam("dest", "-1257786")
                    .queryParam("supplier", supplierId)
                    .queryParam("page", page)
                    .build()
                }
                .retrieve()
                .awaitBody()
        } catch (e: Exception) {
            logger.error("Error: ", e)
            null
        }

    private suspend fun getCard(url: String): WBInfoDto? =
        try {
            webClient.get()
                .uri(url)
                .retrieve()
                .awaitBody()
        } catch (e: Exception) {
            logger.error("Error: ", e)
            null
        }
    private fun getItemId(url: String): String? {
        if (!url.contains("catalog/") && !url.contains("/detail"))
            return null
        val lastPart = url.substring(url.indexOf("catalog/") + 8)
        return lastPart.substring(0, lastPart.indexOf("/detail"))
    }

    private fun getImageUrl(id: Long, pic: String): String {
        val extraShort = id.div(100000)
        val short = id.div(1000)
        return IMAGE_URL.format(getBucket(extraShort), extraShort, short, id, pic)
    }

    private fun getCardUrl(id: Long): String {
        val extraShort = id.div(100000)
        val short = id.div(1000)
        return INFO_URL.format(getBucket(extraShort), extraShort, short, id)
    }

    private fun getBucket(id: Long): String {
        return if (id <= 143L)
            "01"
        else if (id <= 287L)
            "02"
        else if (id <= 431L)
            "03"
        else if (id <= 719L)
            "04"
        else if (id <= 1007L)
            "05"
        else if (id <= 1061L)
            "06"
        else if (id <= 1115L)
            "07"
        else if (id <= 1169L)
            "08"
        else if (id <= 1313L)
            "09"
        else if (id <= 1601L)
            "10"
        else if (id <= 1655L)
            "11"
        else if (id <= 1919L)
            "12"
        else if (id <= 2059L)
            "13"
        else if (id <= 2189L)
            "14"
        else if (id <= 2339L)
            "15"
        else
            "16"
    }
}
