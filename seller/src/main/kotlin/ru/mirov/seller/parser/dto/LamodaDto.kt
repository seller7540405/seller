package ru.mirov.seller.parser.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class LamodaDto (
    var gallery: List<String>? = null,
    @JsonProperty("seo_title")
    var seoTitle: String? = null,
    var sku: String? = null,
    var title: String? = null,
    var price: Long? = null,
    @JsonProperty("model_title")
    var modelTitle: String? = null
)
