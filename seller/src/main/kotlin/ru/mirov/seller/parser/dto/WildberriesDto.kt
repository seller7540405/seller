package ru.mirov.seller.parser.dto

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties
data class WildberriesDetailDto (
    var data: WildberriesDetailData? = null
)

@JsonIgnoreProperties
data class WildberriesCatalogDto (
    var data: WildberriesCatalogData? = null
)

@JsonIgnoreProperties
data class WildberriesDetailData (
    var products: List<WildberriesDetailProducts>? = null
)

@JsonIgnoreProperties
data class WildberriesDetailProducts (
    val name: String,
    val supplierId: Long
)

@JsonIgnoreProperties
data class WildberriesCatalogData (
    var products: MutableList<WildberriesCatalogProducts>? = null,
    var total: Int? = 0
)

@JsonIgnoreProperties
data class WildberriesCatalogProducts (
    val id: Long,
    var salePriceU: Long? = null,
    var pics: Int? = null
)

@JsonIgnoreProperties
data class WBInfoDto (
    var description: String? = null
)
