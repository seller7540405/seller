package ru.mirov.seller.parser.dto

data class GoodDto (
    val name: String,
    var productImages: List<String>? = null,
    var description: String? = null,
    var price: String? = null,
    val articul: String
)
