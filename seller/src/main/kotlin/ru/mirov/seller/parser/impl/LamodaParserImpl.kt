package ru.mirov.seller.parser.impl

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.awaitBody
import ru.mirov.seller.extensions.logger.Loggable
import ru.mirov.seller.parser.MarketPlaceParser
import ru.mirov.seller.parser.dto.GoodDto
import ru.mirov.seller.parser.dto.LamodaDto

@Component("lamoda")
class LamodaParserImpl (
    private val webClient: WebClient,
    @Value("\${application.lamoda.get}") private val getUrl: String
): MarketPlaceParser {
    companion object: Loggable {
        private const val IMAGE_URL = "https://a.lmcdn.ru/img600x866"
    }
    override suspend fun parse(link: String): GoodDto? {
        val id = getItemId(link) ?: return null
        val dto = get(id)
        var name = id
        if (dto?.title != null)
            name += " ${dto.title}"
        if (dto?.modelTitle != null)
            name += " ${dto.modelTitle}"
        return GoodDto(
            articul = id,
            productImages = dto?.gallery?.map { "$IMAGE_URL$it" },
            name = name,
            price = "${dto?.price} ₽",
            description = dto?.seoTitle
        )
    }

    private suspend fun get(id: String): LamodaDto? =
        try {
            webClient.get()
                .uri(getUrl) {
                    u -> u.queryParam("sku", id).build()
                }
                .retrieve()
                .awaitBody()
        } catch (e: Exception) {
            logger.error("Error: ", e)
            null
        }

    private fun getItemId(url: String): String? {
        if (!url.contains("/p/"))
            return null
        val lastPart = url.substring(url.indexOf("/p/") + 3)
        return lastPart.substring(0, lastPart.indexOf("/")).uppercase()
    }
}
