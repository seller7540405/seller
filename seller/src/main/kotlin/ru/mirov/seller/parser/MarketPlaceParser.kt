package ru.mirov.seller.parser

import ru.mirov.seller.parser.dto.GoodDto

interface MarketPlaceParser {
    suspend fun parse(link: String): GoodDto?
}
