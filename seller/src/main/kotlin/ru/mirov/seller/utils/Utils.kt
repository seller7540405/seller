package ru.mirov.seller.utils

import kotlinx.coroutines.*
import kotlinx.coroutines.slf4j.MDCContext
import org.slf4j.MDC
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

fun <T> doLaunch(
    withCurrentMdcContext: Boolean = true,
    mdcContext: MDCContext? = null,
    coroutineDispatcher: CoroutineDispatcher = Dispatchers.IO,
    action: suspend () -> T
) = CoroutineScope(coroutineDispatcher).let {
    return@let when {
        mdcContext != null -> it.launch(mdcContext) { action() }
        withCurrentMdcContext -> it.launch(MDCContext(MDC.getCopyOfContextMap())) { action() }
        else -> it.launch { action() }
    }
}

fun <T> doAsync(
    withCurrentMdcContext: Boolean = true,
    mdcContext: MDCContext? = null,
    coroutineDispatcher: CoroutineDispatcher = Dispatchers.IO,
    action: suspend () -> T
) = CoroutineScope(coroutineDispatcher).let {
    return@let when {
        mdcContext != null -> it.async(mdcContext) { action() }
        withCurrentMdcContext -> it.async(MDCContext(MDC.getCopyOfContextMap())) { action() }
        else -> it.async { action() }
    }
}

fun String.markdown() = this
    .replace("_", "\\_")
    .replace("-", "\\-")
    .replace("!", "\\!")
    .replace(".", "\\.")
    .replace("(", "\\(")
    .replace(")", "\\)")
    .replace("+", "\\+")
    .replace("#", "\\#")
    .replace("=", "\\=")
    .replace("|", "\\|")
    .replace("~", "\\~")
    .replace("`", "\\`")
    .replace("[", "\\[")
    .replace("]", "\\]")
    .replace(">", "\\>")
    .replace("{", "\\{")
    .replace("}", "\\}")
    .replace("@", "\\@")
    .replace("&", "\\&")

fun validatePhone(value: String): Boolean {
    if (value.length != 10)
        return false
    return isNumeric(value)
}

fun validateInn(value: String): Boolean {
    if (value.length != 12)
        return false
    return isNumeric(value)
}

fun validateEmail(value: String): Boolean {
    val emailRegex = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+\$"
    return value.matches(emailRegex.toRegex())
}

private fun isNumeric(value: String): Boolean {
    val regex = "-?[0-9]+(\\.[0-9]+)?".toRegex()
    return value.matches(regex)
}

fun LocalDateTime.formatToString(): String {
    val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")
    return formatter.format(this)
}

fun strToDate(value: String): LocalDateTime {
    var stringDate = value
    if (stringDate.length != 5)
        stringDate = "0$stringDate"
    return LocalDate.parse("$stringDate.2024", DateTimeFormatter.ofPattern("dd.MM.yyyy")).atStartOfDay()
}
