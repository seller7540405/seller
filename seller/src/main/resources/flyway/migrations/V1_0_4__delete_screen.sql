insert into TG_SCREENS
values ('DELETE_ACCOUNT', 'Вы уверены, что хотите удалить вашу учетную запись?', '{
  "type": "COLUMN",
  "items": [
    {
      "name": "Да",
      "action": "delete_account"
    },
    {
      "name": "Нет",
      "action": "remain_account"
    }
  ]
}');

insert into TG_SCREENS
values ('DELETED_ACCOUNT', 'Ваш аккаунт удален, вы можете заново зарегистрироваться в боте в любой момент', null);
