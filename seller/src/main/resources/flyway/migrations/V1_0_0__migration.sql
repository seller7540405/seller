drop table if exists TG_BOT_USERS;
create table TG_BOT_USERS (
   id uuid NOT NULL,
   created_at timestamp NULL,
   updated_at timestamp NULL,
   user_details jsonb
);

drop table if exists OWNER_BUYOUTS;
create table OWNER_BUYOUTS (
    id uuid NOT NULL,
	created_at timestamp NULL,
	updated_at timestamp NULL,
	buyout_details jsonb
);

drop table if exists BUYOUT_ACTIONS;
create table BUYOUT_APPEALS (
   id uuid NOT NULL,
   created_at timestamp NULL,
   updated_at timestamp NULL,
   ACTION_DETAILS jsonb
);

drop table if exists FAQ;
create table FAQ (
    id uuid NOT NULL,
    created_at timestamp NULL,
    updated_at timestamp NULL,
    faq_details jsonb
);



drop table if exists CLIENT_BUYOUTS;
create table CLIENT_BUYOUTS (
                                id uuid NOT NULL,
                                created_at timestamp NULL,
                                updated_at timestamp NULL,
                                client_details jsonb
);
