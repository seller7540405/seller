insert into TG_SCREENS
values ('INSTRUCTION_SCREEN', 'Выберите инструкцию которая Вас интересует', '{
  "type": "COLUMN",
  "items": [
    {
      "name": "Продавец/Селлер",
      "action": "seller_instruction"
    },
    {
      "name": "Покупатель/Блогер",
      "action": "buyer_instruction"
    }
  ]
}');
