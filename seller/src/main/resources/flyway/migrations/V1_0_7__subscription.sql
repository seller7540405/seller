drop table if exists BUYER_SUBSCRIPTION;
create table BUYER_SUBSCRIPTION (
  id uuid NOT NULL,
  created_at timestamp NULL,
  updated_at timestamp NULL,
  tg_id varchar,
  search_value varchar null
);

insert into TG_SCREENS
values ('CREATE_SUBSCRIPTION', 'Наш бот будет искать для вас подходящие товары и отправлять каждый день в 12:00 по Москве.' ||
                               'Напишите, какие товары Вас интересуют написав в начале слово "Искать" (например: Искать кроссовки addidas)' ||
                               ' или нажмите "Искать все"', '{
  "type": "ROW",
  "items": [
    {
      "name": "Искать все",
      "action": "search_all_subs"
    },{
      "name": "Удалить подписки",
      "action": "delete_all_subs"
    }
  ]
}');

insert into TG_SCREENS
values ('CREATED_SUBSCRIPTION', 'Ваша подписка создана', null);

insert into TG_SCREENS
values ('DELETED_SUBSCRIPTION', 'Ваша подписка удалена', null);
