drop table if exists TG_SCREENS;
create table TG_SCREENS (
    id varchar primary key,
    title varchar,
    buttons jsonb
);


insert into TG_SCREENS
values ('CHOOSE_TYPE', 'Привет!
Для начала пользованием ботом, выберите роль и зарегистрируйтесь', '{
  "type": "COLUMN",
  "items": [
    {
      "name": "Продавец",
      "action": "register_seller"
    },
    {
      "name": "Покупатель",
      "action": "register_buyer"
    }
  ]
}');

insert into TG_SCREENS
values ('REGISTER_BUYER', 'Необходимо зарегистрироваться, введите свой номер телефона в формате 9261344598', null);

insert into TG_SCREENS
values ('REGISTER_SELLER', 'Необходимо зарегистрироваться, введите свой номер телефона в формате 9261344598.
Важно указать правильный номер, на него будет осуществляться перевод денежных средств продавцом.', null);

insert into TG_SCREENS
values ('NEED_EMAIL', 'Введите свой email', null);

insert into TG_SCREENS
values ('NEED_INN', 'Введите свой ИНН', null);

insert into TG_SCREENS
values ('SELLER_CHOOSE_ACTION', 'Выберите действие', '{
  "type": "ROW",
  "items": [
    {
      "name": "Добавить выкуп",
      "action": "add_buyout"
    },
    {
      "name": "Список моих товаров",
      "action": "my_goods"
    },
    {
      "name": "Мои заявки на выкуп",
      "action": "my_buyout"
    }
  ]
}');



insert into TG_SCREENS
values ('NEED_INSTAGRAM', 'Укажите ваш аккаунт в instagram (например ссылку (https://www.instagram.com/Mirov_artur) или никнейм через @ (@Mirov_artur))', null);

insert into TG_SCREENS
values ('BUYER_CHOOSE_ACTION', 'Выберите действие', '{
  "type": "ROW",
  "items": [
    {
      "name": "Получить 10 актуальных товаров",
      "action": "get_available_buyouts"
    },
    {
      "name": "Свободный поиск товаров",
      "action": "search_available_buyouts"
    }
  ]
}');




insert into TG_SCREENS
values ('SELLER_PRODUCT_ADD_LINK', 'Добавьте ссылку на товар из маркетплейса для размещения на выкуп.',null);

insert into TG_SCREENS
values ('SELLER_PRODUCT_ADD_LINK_SAVE', '%owner_buyouts.name+description+product_price+product_images%', '{
  "type": "ROW",
  "items": [
    {
      "name": "Сохранить описание товара ✓",
      "action": "save_new_product"
    }
  ]
}');

insert into TG_SCREENS
values ('SELLER_PRODUCT_ADD_VIDEOTZ', '%owner_buyouts.name+description+product_price+product_images%', '{
  "type": "ROW",
  "items": [
    {
      "name": "Прикрепить видео",
      "action": "upload_product_videotz"
    }
  ]
}');

insert into TG_SCREENS
values ('SELLER_PRODUCT_ADD_VIDEOTZ_CONFIRM', '%owner_buyouts.name+description+product_price+product_images%', '{
  "type": "ROW",
  "items": [
    {
      "name": "Видео ТЗ выкупа",
      "action": "open_product_videotz"
    },
    {
      "name": "Удалить видео",
      "action": "delete_product_videotz"
    }
  ]
}');

insert into TG_SCREENS
values ('SELLER_PRODUCT_ADD_START_DATE_INPUT', 'Укажите дату начала кампании в формате *DD.MM* по продвижению', null);

insert into TG_SCREENS
values ('SELLER_PRODUCT_ADD_START_DATE_CONFIRM_END_DATE_INPUT', 'Укажите дату окончания кампании по продвижению в формате *DD.MM*', null);

insert into TG_SCREENS
values ('SELLER_PRODUCT_ADD_END_DATE_CONFIRM_COUNT_INPUT', 'Укажите количество товаров, доступных ежедневно для выкупа. В формате числа, например 25', null);

insert into TG_SCREENS
values ('SELLER_PRODUCT_ADD_COUNT_CONFIRM', 'Укажите дату начала кампании в формате DD.MM по продвижению или нажмите "Начать сейчас"', '{
  "type": "ROW",
  "items": [
    {
      "name": "Активировать ✓",
      "action": "activate"
    }
  ]
}');

insert into TG_SCREENS
values ('SELLER_DONE_BUYOUTS_LIST', 'Список завершенных выкупов - %param из draw.io%. Отображается название товара и дата завершения.', null);

insert into TG_SCREENS
values ('SELLER_DONE_BUYOUT_CARD', '%owner_buyouts.name+description+product_price+product_images%', '{
  "type": "ROW",
  "items": [
    {
      "name": "%owner_buyouts.name + client_buyouts.update_at%",
      "action": "seller_done_buyout_card"
    },
    {
      "name": "Видео ТЗ выкупа",
      "action": "open_product_videotz"
    },
    {
      "name": "Фото заказа № %N%",
      "action": "open_order_image_proof"
    },
    {
      "name": "Фото выкупа № %N%",
      "action": "open_buying_image_proof"
    }
  ]
}');

insert into TG_SCREENS
values ('SELLER_OPEN_BUYOUTS_LIST', 'Список открытых выкупов - %param из draw.io%. Отображается название товара и количество товара.', null);

insert into TG_SCREENS
values ('SELLER_BUYOUT_STATISTIC_CARD', '%draw.io product params%', '{
  "type": "ROW",
  "items": [
    {
      "name": "Описание товара",
      "action": "open_product_card"
    },
    {
      "name": "Видео ТЗ выкупа",
      "action": "open_product_videotz"
    },
    {
      "name": "Выкупы на проверку",
      "action": "buyouts_ready_to_proof"
    },
    {
      "name": "Удалить товар",
      "action": "delete_product"
    },
    {
      "name": "Изменить дату окончания выкупа",
      "action": "change_product_end_date"
    },
    {
      "name": "Изменить кол-во выкупов/день",
      "action": "change_product_count_per_day"
    }
  ]
}');

insert into TG_SCREENS
values ('SELLER_PRODUCT_ADD_NAME', 'Укажите имя товара', null);

insert into TG_SCREENS
values ('SELLER_PRODUCT_ADD_DESCRIPTION', 'Укажите описание товара', null);

insert into TG_SCREENS
values ('SELLER_PRODUCT_DESCRIPTION_TZ', 'Опишите инструкцию (ТЗ) для выкупа вашего товара.
Пример ТЗ.
1.Введите в строку поиска Валдбериз запрос Футболка
2. Сделайте фильтр по бренду Арко
3. Какой формат рекламы вам нужен Рилс или Сторис (Если нужна определенная подача укажите это)
3. Пишите мне лс когда сможете сделать рекламу
4. Нужно/ не нужно оставлять отзыв на WB
5. Оплата товара после выхода рекламы и скрина отзыва на WB', null);

insert into TG_SCREENS
values ('ELASTIC_SEARCH_BUYOUTS', 'Для начала поиска, введите что вас интересует (Например: Футболка мужская, nike)', null);
