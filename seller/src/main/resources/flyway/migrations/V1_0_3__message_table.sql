drop table if exists tg_message;
create table tg_message (
   id uuid NOT NULL,
   created_at timestamp NULL,
   updated_at timestamp NULL,
   message_id bigint,
   tg_id varchar
);
