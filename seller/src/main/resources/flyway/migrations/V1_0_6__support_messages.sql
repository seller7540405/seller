drop table if exists SUPPORT_MESSAGES;
create table SUPPORT_MESSAGES (
  id uuid NOT NULL,
  created_at timestamp NULL,
  updated_at timestamp NULL,
  message_detail jsonb
);

insert into TG_SCREENS
values ('SUPPORT_MESSAGES', 'Введите вопрос, который вас интересует и наша поддержка свяжется с Вами в ближайшее время', null);

insert into TG_SCREENS
values ('SUPPORT_MESSAGES_CREATE', 'Спасибо! Ваш вопрос передан в поддержку.', null);
