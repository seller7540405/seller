plugins {
    id("org.springframework.boot") version "3.2.2"
    id("io.spring.dependency-management") version "1.1.4"

    val kotlinVersion = "1.9.23"
    kotlin("jvm") version kotlinVersion
    kotlin("plugin.spring") version kotlinVersion
}

group = "ru.mirov"
version = "1.5.1"

java {
    sourceCompatibility = JavaVersion.VERSION_21
}

repositories {
    mavenCentral()
    maven { // smft nexus url
        url = uri("https://jitpack.io")
    }
    mavenLocal()
}

dependencies {
    //spring
    implementation("org.springframework.boot:spring-boot-starter-webflux")

    //kotlin
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("io.projectreactor.kotlin:reactor-kotlin-extensions:1.2.2")
    implementation("org.jetbrains.kotlin:kotlin-reflect:1.9.22")
    val kotlinxGroup = "org.jetbrains.kotlinx"
    val kotlinxVersion = "1.7.3"
    implementation("$kotlinxGroup:kotlinx-coroutines-reactor:$kotlinxVersion")
    implementation("$kotlinxGroup:kotlinx-coroutines-core:$kotlinxVersion")
    implementation("$kotlinxGroup:kotlinx-coroutines-slf4j:$kotlinxVersion")

    //db
    val postgresqlGroup = "org.postgresql"
    implementation("$postgresqlGroup:r2dbc-postgresql")
    implementation("$postgresqlGroup:postgresql")
    implementation("org.springframework.data:spring-data-r2dbc:3.2.2")
    implementation("io.r2dbc:r2dbc-pool")
    val flywayGroup = "org.flywaydb"
    val flywayVersion = "10.7.1"
    implementation("$flywayGroup:flyway-core:$flywayVersion")
    implementation("$flywayGroup:flyway-database-postgresql:$flywayVersion")

    //jackson
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.16.0-rc1")
    implementation("com.fasterxml.uuid:java-uuid-generator:4.3.0")

    //telegram
    val tgGroup = "org.telegram"
    val tgVersion = "6.9.7.0"
    implementation("$tgGroup:telegrambots:$tgVersion")
    implementation("$tgGroup:telegrambots-spring-boot-starter:$tgVersion")
    implementation("$tgGroup:telegrambotsextensions:$tgVersion")
    implementation("$tgGroup:telegrambots-abilities:$tgVersion")

    implementation("org.liquibase:liquibase-core")
}


tasks.jar {
    enabled = false
}

tasks.compileKotlin {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict", "-Xjvm-default=all")
        jvmTarget = "21"
    }
}

springBoot {
    buildInfo()
}
